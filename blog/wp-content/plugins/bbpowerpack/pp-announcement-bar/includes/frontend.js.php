(function($) {
    if( $('.fl-node-<?php echo $id; ?> .pp-announcement-bar-wrap').hasClass('bottom') ) {
        $('html').addClass('pp-bottom-bar');
    }
    if( $('.fl-node-<?php echo $id; ?> .pp-announcement-bar-wrap').hasClass('top') ) {
        if( $('body').hasClass('admin-bar') ) {
            $('html').addClass('pp-announcement-bar pp-top-bar pp-top-bar-admin');
        }
        else {
            $('html').addClass('pp-announcement-bar pp-top-bar');
        }
    }

    <?php if( $settings->announcement_bar_position == 'bottom' ) { ?>
        $('.fl-node-<?php echo $id; ?> .pp-announcement-bar-close-button').on('click', function() {
            $('html').removeClass('pp-bottom-bar');
        });
    <?php } ?>
    <?php if( $settings->announcement_bar_position == 'top' ) { ?>
        $('.fl-node-<?php echo $id; ?> .pp-announcement-bar-close-button').on('click', function() {
            $('html').removeClass('pp-top-bar');
            $('html').removeClass('pp-top-bar-admin');
        });
    <?php } ?>

})(jQuery);
