(function($) {
    jQuery(document).ready(function($){
    	var $timeline_block = $('.pp-timeline-item');

    	//hide timeline blocks which are outside the viewport
    	$timeline_block.each(function(){
    		if($(this).offset().top > $(window).scrollTop()+$(window).height()*0.75) {
    			$(this).find('.pp-timeline-content').addClass('pp-is-hidden');
    			$(this).find('.pp-separator-arrow').addClass('pp-is-hidden');
    		}
    	});

    	//on scolling, show/animate timeline blocks when enter the viewport
    	$(window).on('scroll', function(){
    		$timeline_block.each(function(){
    			if( $(this).offset().top <= $(window).scrollTop()+$(window).height()*0.75 && $(this).find('.pp-timeline-content').hasClass('pp-is-hidden') ) {
    				$(this).find('.pp-timeline-content').removeClass('pp-is-hidden').addClass('pp-fade');
    				$(this).find('.pp-separator-arrow').addClass('pp-fade');
    			}
    		});
    	});
    });
})(jQuery);
