<?php

$infobox_class = 'pp-infobox-wrap';

?>
<div class="<?php echo $infobox_class; ?>">
	<?php

	$layout = $settings->layouts;
	switch ( $layout ) {
		case '1': ?>
			<?php if( $settings->pp_infobox_link_type == 'box' ) { ?>
				<a class="pp-more-link" href="<?php echo $settings->link; ?>">
			<?php } ?>
			<div class="pp-infobox layout-1 clearfix">
				<div class="pp-heading-wrapper">
					<div class="pp-icon-wrapper animated">
						<?php if( $settings->icon_type == 'icon' ) { ?>
							<div class="pp-infobox-icon">
								<div class="pp-infobox-icon-inner">
									<span class="pp-icon <?php echo $settings->icon_select; ?>"></span>
								</div>
							</div>
						<?php } else { ?>
							<div class="pp-infobox-image">
								<img src="<?php echo wp_get_attachment_url( absint($settings->image_select) ); ?>">
							</div>
						<?php } ?>
					</div>
					<?php if( $settings->pp_infobox_link_type == 'title' ) { ?>
						<a class="pp-more-link pp-title-link" href="<?php echo $settings->link; ?>">
					<?php } ?>
					<div class="pp-infobox-title-wrapper">
						<p class="pp-infobox-title"><?php echo $settings->title; ?></p>
					</div>
					<?php if( $settings->pp_infobox_link_type == 'title' ) { ?>
						</a>
					<?php } ?>
				</div>
				<div class="pp-infobox-description">
					<?php echo $settings->description; ?>
					<?php if( $settings->pp_infobox_link_type == 'read_more' ) { ?>
						<p><a class="pp-more-link" href="<?php echo $settings->link; ?>"><?php echo $settings->pp_infobox_read_more_text; ?></a></p>
					<?php } ?>
				</div>
			</div>
			<?php if( $settings->pp_infobox_link_type == 'box' ) { ?>
				</a>
			<?php }
			break;

		case '2': ?>
		<?php if( $settings->pp_infobox_link_type == 'box' ) { ?>
			<a class="pp-more-link" href="<?php echo $settings->link; ?>">
		<?php } ?>
			<div class="pp-infobox layout-2 clearfix">
				<div class="pp-heading-wrapper">
					<?php if( $settings->pp_infobox_link_type == 'title' ) { ?>
						<a class="pp-more-link pp-title-link" href="<?php echo $settings->link; ?>">
					<?php } ?>
					<div class="pp-infobox-title-wrapper">
						<p class="pp-infobox-title"><?php echo $settings->title; ?></p>
					</div>
					<?php if( $settings->pp_infobox_link_type == 'title' ) { ?>
						</a>
					<?php } ?>
					<div class="pp-icon-wrapper animated">
						<?php if( $settings->icon_type == 'icon' ) { ?>
							<div class="pp-infobox-icon">
								<div class="pp-infobox-icon-inner">
									<span class="pp-icon <?php echo $settings->icon_select; ?>"></span>
								</div>
							</div>
						<?php } else { ?>
							<div class="pp-infobox-image">
								<img src="<?php echo wp_get_attachment_url( absint($settings->image_select) ); ?>">
							</div>
						<?php } ?>
					</div>
				</div>
				<div class="pp-infobox-description">
					<?php echo $settings->description; ?>
					<?php if( $settings->pp_infobox_link_type == 'read_more' ) { ?>
						<a class="pp-more-link" href="<?php echo $settings->link; ?>"><?php echo $settings->pp_infobox_read_more_text; ?></a>
					<?php } ?>
				</div>
			</div>
			<?php if( $settings->pp_infobox_link_type == 'box' ) { ?>
				</a>
			<?php }

			break;

		case '3': ?>
		<?php if( $settings->pp_infobox_link_type == 'box' ) { ?>
			<a class="pp-more-link" href="<?php echo $settings->link; ?>">
		<?php } ?>
			<div class="pp-infobox layout-3 clearfix">
				<div class="layout-3-wrapper">
					<div class="pp-icon-wrapper animated">
						<?php if( $settings->icon_type == 'icon' ) { ?>
							<div class="pp-infobox-icon">
								<div class="pp-infobox-icon-inner">
									<span class="pp-icon <?php echo $settings->icon_select; ?>"></span>
								</div>
							</div>
						<?php } else { ?>
							<div class="pp-infobox-image">
								<img src="<?php echo wp_get_attachment_url( absint($settings->image_select) ); ?>">
							</div>
						<?php } ?>
					</div>
					<div class="pp-heading-wrapper">
							<?php if( $settings->pp_infobox_link_type == 'title' ) { ?>
								<a class="pp-more-link" href="<?php echo $settings->link; ?>">
							<?php } ?>
								<div class="pp-infobox-title-wrapper">
									<p class="pp-infobox-title"><?php echo $settings->title; ?></p>
								</div>
							<?php if( $settings->pp_infobox_link_type == 'title' ) { ?>
								</a>
							<?php } ?>
						<div class="pp-infobox-description">
							<?php echo $settings->description; ?>
							<?php if( $settings->pp_infobox_link_type == 'read_more' ) { ?>
								<a class="pp-more-link" href="<?php echo $settings->link; ?>"><?php echo $settings->pp_infobox_read_more_text; ?></a>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<?php if( $settings->pp_infobox_link_type == 'box' ) { ?>
				</a>
			<?php }
			break;

		case '4': ?>
		<?php if( $settings->pp_infobox_link_type == 'box' ) { ?>
			<a class="pp-more-link" href="<?php echo $settings->link; ?>">
		<?php } ?>
			<div class="pp-infobox layout-4 clearfix">
				<div class="layout-4-wrapper">
					<div class="pp-heading-wrapper">
							<?php if( $settings->pp_infobox_link_type == 'title' ) { ?>
								<a class="pp-more-link" href="<?php echo $settings->link; ?>">
							<?php } ?>
								<div class="pp-infobox-title-wrapper">
									<p class="pp-infobox-title"><?php echo $settings->title; ?></p>
								</div>
							<?php if( $settings->pp_infobox_link_type == 'title' ) { ?>
								</a>
							<?php } ?>
						<div class="pp-infobox-description">
							<?php echo $settings->description; ?>
							<?php if( $settings->pp_infobox_link_type == 'read_more' ) { ?>
								<a class="pp-more-link" href="<?php echo $settings->link; ?>"><?php echo $settings->pp_infobox_read_more_text; ?></a>
							<?php } ?>
						</div>
					</div>
					<div class="pp-icon-wrapper animated">
						<?php if( $settings->icon_type == 'icon' ) { ?>
							<div class="pp-infobox-icon">
								<div class="pp-infobox-icon-inner">
									<span class="pp-icon <?php echo $settings->icon_select; ?>"></span>
								</div>
							</div>
						<?php } else { ?>
							<div class="pp-infobox-image">
								<img src="<?php echo wp_get_attachment_url( absint($settings->image_select) ); ?>">
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<?php if( $settings->pp_infobox_link_type == 'box' ) { ?>
				</a>
			<?php }
			break;

		case '5': ?>
		<?php if( $settings->pp_infobox_link_type == 'box' ) { ?>
			<a class="pp-more-link" href="<?php echo $settings->link; ?>">
		<?php } ?>
			<div class="pp-infobox layout-5 clearfix">
				<div class="pp-icon-wrapper animated">
					<?php if( $settings->icon_type == 'icon' ) { ?>
						<div class="pp-infobox-icon">
							<div class="pp-infobox-icon-inner">
								<span class="pp-icon <?php echo $settings->icon_select; ?>"></span>
							</div>
						</div>
					<?php } else { ?>
						<div class="pp-infobox-image">
							<img src="<?php echo wp_get_attachment_url( absint($settings->image_select) ); ?>">
						</div>
					<?php } ?>
				</div>
				<?php if( $settings->pp_infobox_link_type == 'title' ) { ?>
					<a class="pp-more-link" href="<?php echo $settings->link; ?>">
				<?php } ?>
				<div class="pp-infobox-title-wrapper">
					<p class="pp-infobox-title"><?php echo $settings->title; ?></p>
				</div>
				<?php if( $settings->pp_infobox_link_type == 'title' ) { ?>
					</a>
				<?php } ?>
				<div class="pp-infobox-description">
					<?php echo $settings->description; ?>
					<?php if( $settings->pp_infobox_link_type == 'read_more' ) { ?>
						<a class="pp-more-link" href="<?php echo $settings->link; ?>"><?php echo $settings->pp_infobox_read_more_text; ?></a>
					<?php } ?>
				</div>
			</div>
			<?php if( $settings->pp_infobox_link_type == 'box' ) { ?>
				</a>
			<?php }
			break;

		default: ?>
		<?php if( $settings->pp_infobox_link_type == 'box' ) { ?>
			<a class="pp-more-link" href="<?php echo $settings->link; ?>">
		<?php } ?>
			<div class="pp-infobox clearfix">
				<div class="pp-icon-wrapper animated">
					<?php if( $settings->icon_type == 'icon' ) { ?>
						<div class="pp-infobox-icon">
							<div class="pp-infobox-icon-inner">
								<span class="pp-icon <?php echo $settings->icon_select; ?>"></span>
							</div>
						</div>
					<?php } else { ?>
						<div class="pp-infobox-image">
							<img src="<?php echo wp_get_attachment_url( absint($settings->image_select) ); ?>">
						</div>
					<?php } ?>
				</div>
				<?php if( $settings->pp_infobox_link_type == 'title' ) { ?>
					<a class="pp-more-link pp-title-link" href="<?php echo $settings->link; ?>">
				<?php } ?>
				<div class="pp-infobox-title-wrapper">
					<p class="pp-infobox-title"><?php echo $settings->title; ?></p>
				</div>
				<?php if( $settings->pp_infobox_link_type == 'title' ) { ?>
					</a>
				<?php } ?>
				<div class="pp-infobox-description">
					<?php echo $settings->description; ?>
					<?php if( $settings->pp_infobox_link_type == 'read_more' ) { ?>
						<a class="pp-more-link" href="<?php echo $settings->link; ?>"><?php echo $settings->pp_infobox_read_more_text; ?></a>
					<?php } ?>
				</div>
			</div>
			<?php if( $settings->pp_infobox_link_type == 'box' ) { ?>
				</a>
			<?php }
			break;
	}
	?>

</div>
