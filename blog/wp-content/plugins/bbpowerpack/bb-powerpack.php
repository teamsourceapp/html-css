<?php
/**
 * Plugin Name: PowerPack for Beaver Builder
 * Plugin URI: https://www.wpbeaveraddons.com
 * Description: A set of custom, creative, unique modules for Beaver Builder to speed up your web design and development process.
 * Version: 1.0.9
 * Author: Team IdeaBox - WP Beaver Addons
 * Author URI: https://www.wpbeaveraddons.com
 * Copyright: (c) 2016 IdeaBox Creations
 * License: GNU General Public License v2.0
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: bb-powerpack
 */

define( 'BB_POWERPACK_DIR', plugin_dir_path( __FILE__ ) );
define( 'BB_POWERPACK_URL', plugins_url( '/', __FILE__ ) );
define( 'BB_POWERPACK_CAT', bb_powerpack_wl() );

/**
 * Modules and Fields
 */
function bb_powerpack_load_files() {
	if ( class_exists( 'FLBuilder' ) ) {
		require_once 'fields.php';
		require_once 'updater/update-config.php';
	    require_once 'pp-gravity-form/pp-gravity-form.php';
		require_once 'pp-highlight-box/pp-highlight-box.php';
	    require_once 'pp-testimonials/pp-testimonials.php';
		require_once 'pp-info-banner/pp-info-banner.php';
	    require_once 'pp-row-effects/pp-row-effects.php';
	    require_once 'pp-infolist/pp-infolist.php';
	    require_once 'pp-flipbox/pp-flipbox.php';
	    require_once 'pp-infobox/pp-infobox.php';
	    require_once 'pp-modal-box/pp-modal-box.php';
	    require_once 'pp-column-separator/pp-column-separator.php';
	    require_once 'pp-dotnav/pp-dotnav.php';
	    require_once 'pp-line-separator/pp-line-separator.php';
		require_once 'pp-heading/pp-heading.php';
		require_once 'pp-contact-form-7/pp-contact-form-7.php';
		require_once 'pp-logos-grid/pp-logos-grid.php';
		require_once 'pp-timeline/pp-timeline.php';
		require_once 'pp-notifications/pp-notifications.php';
		require_once 'pp-pullquote/pp-pullquote.php';
		require_once 'pp-announcement-bar/pp-announcement-bar.php';
		require_once 'pp-hover-cards/pp-hover-cards.php';
		require_once 'pp-smart-button/pp-smart-button.php';
		require_once 'pp-image-panels/pp-image-panels.php';
		require_once 'pp-dual-button/pp-dual-button.php';
		require_once 'pp-image/pp-image.php';
	}
}
add_action( 'init', 'bb_powerpack_load_files' );

function bb_powerpack_ui_panel() {
	require_once 'panel/panel-functions.php';
}
add_action( 'plugins_loaded', 'bb_powerpack_ui_panel' );

/**
 * Custom scripts
 */
function bb_powerpack_load_scripts() {

	wp_enqueue_style( 'animate', plugins_url( 'assets/css/animate.min.css', __FILE__ ), array(), rand() );

}
add_action( 'wp_enqueue_scripts', 'bb_powerpack_load_scripts', 100 );

/**
 * Enable white labeling setting form after re-activating the plugin
 */
function bb_powerpack_option_reset() {
	delete_option( 'ppwl_hide_form' );
	delete_option( 'ppwl_hide_plugin' );
}
register_activation_hook( __FILE__, 'bb_powerpack_option_reset' );

function bb_powerpack_wl() {
	if (get_option('ppwl_builder_label') && '' != get_option('ppwl_builder_label')) {
		$ppwl = get_option('ppwl_builder_label');
	} else {
		$ppwl = __('PowerPack Modules', 'bb-powerpack');
	}

	return $ppwl;
}

/**
 * Include files
 */
require_once 'extensions/row.php';
