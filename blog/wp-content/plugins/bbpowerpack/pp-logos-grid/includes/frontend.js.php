
(function($) {

<?php if( $settings->logos_layout == 'carousel' ) { ?>
	// Clear the controls in case they were already created.
	$('.fl-node-<?php echo $id; ?> .logo-slider-next').empty();
	$('.fl-node-<?php echo $id; ?> .logo-slider-prev').empty();

	// Create the slider.
	$('.fl-node-<?php echo $id; ?> .pp-logos-wrapper').bxSlider({
        slideWidth: <?php echo $settings->logo_carousel_width; ?>,
		slideMargin: <?php echo $settings->logos_carousel_spacing; ?>,
        minSlides: <?php echo $settings->logo_carousel_minimum_grid; ?>,
        maxSlides: <?php echo $settings->logo_carousel_maximum_grid; ?>,
		autoStart : <?php echo $settings->logo_slider_auto_play ?>,
		auto : true,
		autoHover: <?php echo $settings->logo_slider_pause_hover; ?>,
		adaptiveHeight: true,
		pause : <?php echo $settings->logo_slider_pause * 1000; ?>,
		mode : '<?php echo $settings->logo_slider_transition; ?>',
		speed : <?php echo $settings->logo_slider_speed * 1000;  ?>,
		pager : <?php echo $settings->logo_slider_dots; ?>,
		nextSelector : '.fl-node-<?php echo $id; ?> .logo-slider-next',
		prevSelector : '.fl-node-<?php echo $id; ?> .logo-slider-prev',
		nextText: '<i class="fa fa-chevron-circle-right"></i>',
		prevText: '<i class="fa fa-chevron-circle-left"></i>',
		controls : <?php echo $settings->logo_slider_arrows; ?>,
		onSliderLoad: function() {
			$('.fl-node-<?php echo $id; ?> .pp-logos-wrapper').addClass('pp-logos-wrapper-loaded');
		}
	});
    <?php } ?>

	function equalheight() {
	    $('.pp-logos-wrapper').each(function(index) {
	        var maxHeight = 0;
	        $(this).children().each(function(index) {
	            if($(this).height() > maxHeight)
	                maxHeight = $(this).height();
	        });
	        $(this).children().height(maxHeight);
	    });
	}
	$(window).bind("load", equalheight);
	$(window).bind("resize", equalheight);

})(jQuery);
