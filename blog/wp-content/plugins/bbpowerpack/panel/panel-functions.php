<?php

function pp_panel_scripts() {

    if ( class_exists( 'FLBuilderModel' ) && FLBuilderModel::is_builder_active() ) {
        wp_enqueue_style( 'pp-panel-style', BB_POWERPACK_URL . 'assets/css/panel.css', array(), rand() );
        wp_enqueue_script( 'pp-panel-script', BB_POWERPACK_URL . 'assets/js/panel.js', array( 'jquery' ), rand(), true );
    }
}
add_action( 'wp_enqueue_scripts', 'pp_panel_scripts', 100 );

function pp_templates_exclude( $data ) {
    if ( isset( $data['categorized'] ) ) {

        $parent = get_term_by( 'slug', 'pp-templates', 'fl-builder-template-category' );
        if ( !is_object( $parent ) ) {
            return;
        }
        $cats = get_term_children( $parent->term_id, 'fl-builder-template-category' );

        foreach ( $cats as $cat ) {
            $term = get_term_by( 'id', $cat, 'fl-builder-template-category' );
            if ( isset( $data['categorized'][$term->slug] ) ) {
                unset( $data['categorized'][$term->slug] );
            }
        }

        if ( isset( $data['categorized']['pp-templates'] ) ) {
            unset( $data['categorized']['pp-templates'] );
        }
        if ( isset( $data['categorized']['uncategorized'] ) ) {
            unset( $data['categorized']['uncategorized'] );
        }
    }

    return $data;
}
//add_filter( 'fl_builder_row_templates_data', 'pp_templates_exclude' );

function pp_templates_get_data() {
    $categorized = array();
    $parent = get_term_by( 'slug', 'pp-templates', 'fl-builder-template-category' );
    if ( !is_object( $parent ) ) {
        return;
    }
    $cats = get_term_children( $parent->term_id, 'fl-builder-template-category' );

    foreach ( $cats as $cat ) {
        $term = get_term_by( 'id', $cat, 'fl-builder-template-category' );
        $data = get_posts( array(
            'post_type'     => 'fl-builder-template',
            'posts_per_page'    => -1,
            'post_status'       => 'publish',
            'tax_query'         => array(
                array(
                    'taxonomy'      => 'fl-builder-template-category',
                    'field'         => 'slug',
                    'terms'         => array($term->slug)
                )
            )
        ) );

        $categorized['categorized'][$term->slug]['name'] = $term->name;

        foreach ( $data as $template ) {
            $categorized['categorized'][$term->slug]['templates'][] = array(
                'id'    => $template->ID,
                'name'  => $template->post_title,
                'image' => wp_get_attachment_url( get_post_thumbnail_id($template->ID) ),
                'type'  => 'user'
            );
        }
    }

    return $categorized;
}

function pp_templates_ui_bar_button( $buttons ) {
    $simple_ui = ! FLBuilderModel::current_user_has_editing_capability();
    $buttons['pp-add-template'] = array(
		'label' => __( 'Add Section', 'bb-powerpack' ),
        'class' => 'pp-add-template-button',
		'show'	=> ! $simple_ui
	);

    return $buttons;
}
//add_filter( 'fl_builder_ui_bar_buttons', 'pp_templates_ui_bar_button' );

function pp_templates_ui_panel() {
    if ( FLBuilderModel::is_builder_active() ) {
        $row_templates = pp_templates_get_data();
        $render_panel = FLBuilderModel::current_user_has_editing_capability();
		if ( $render_panel ) {
			include BB_POWERPACK_DIR . 'panel/ui-panel.php';
		}
    }
}
//add_action( 'wp_footer', 'pp_templates_ui_panel' );

function pp_preview_button() {
    if ( FLBuilderModel::is_builder_active() ) {
    ?>
    <div class="pp-preview-button">
        <div class="pp-preview-button-wrap">
            <span class="pp-preview-trigger fa fa-eye"></span>
        </div>
    </div>
    <?php
    }
}
add_action( 'wp_footer', 'pp_preview_button' );
