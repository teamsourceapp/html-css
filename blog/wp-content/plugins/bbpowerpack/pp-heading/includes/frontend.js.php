/**
 * $module An instance of your module class.
 * $id The module's ID.
 * $settings The module's settings.
*/

(function($) {

    if($(window).width() <= 768 && $(window).width() >= 481 ) {
        $('.pp-heading-separator, .pp-heading').removeClass('<?php echo $settings->heading_alignment; ?>');
        $('.pp-heading-separator, .pp-heading').addClass('pp-tablet-<?php echo $settings->heading_tablet_alignment; ?>');
    }

    if( $(window).width() <= 480 ) {
        $('.pp-heading-separator, .pp-heading').addClass('pp-mobile-<?php echo $settings->heading_mobile_alignment; ?>');
    }

})(jQuery);
