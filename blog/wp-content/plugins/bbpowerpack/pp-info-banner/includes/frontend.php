<?php

/**
 * This file should be used to render each module instance.
 * You have access to two variables in this file:
 *
 * $module An instance of your module class.
 * $settings The module's settings.
 *
 * PPInfoBannerModule:
 */

?>
<div class="pp-info-banner-content <?php echo $settings->banner_image_arrangement; ?>">
	<div class="pp-info-banner-inner">
		<?php if( $settings->banner_image_arrangement == 'static' && $settings->banner_image ) { ?>
		<img src="<?php echo wp_get_attachment_url( absint($settings->banner_image) ); ?>" class="img-<?php echo $settings->banner_image_alignment; ?> animated <?php echo $settings->banner_image_effect; ?>" />
		<?php } ?>
		<div class="info-banner-wrap <?php echo $settings->banner_info_alignment; ?> animated <?php echo $settings->banner_info_animation; ?>">
			<div class="banner-title"><?php echo $settings->banner_title; ?></div>
			<div class="banner-description"><?php echo $settings->banner_description; ?></div>
			<a class="banner-button" href="<?php echo $settings->button_link; ?>" target="<?php echo $settings->button_target; ?>">
				<?php echo $settings->button_text; ?>
			</a>
		</div>
	</div>
</div>
