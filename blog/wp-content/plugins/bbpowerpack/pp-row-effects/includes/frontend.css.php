.fl-node-<?php echo $id; ?> .pp-row-separator .pp-zigzag:before,
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-zigzag:after,
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-pyramid:before,
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-pyramid:after,
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-box-separator:before,
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-box-separator:after {
	height: <?php echo $settings->separator_height; ?>px;
}


.fl-row-bg-overlay .pp-custom-row-<?php echo $id; ?> .fl-row-content {
	position: static;
}

<?php if( $settings->separator_position == 'top' ) { ?>
.fl-node-<?php echo $id; ?> .pp-row-effects-wrap {
	bottom: auto;
	top: 0;
}
.fl-node-<?php echo $id; ?> .pp-row-separator svg {
	bottom: auto;
	top: 0;
}
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-large-triangle-left,
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-tilt-left-separator {
	-webkit-transform: scaleY(-1);
	-moz-transform: scaleY(-1);
	-o-transform: scaleY(-1);
	-ms-transform: scaleY(-1);
	transform: scaleY(-1);
}
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-tilt-right-separator,
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-large-triangle-right {
	-webkit-transform: scale(-1);
	-moz-transform: scale(-1);
	-o-transform: scale(-1);
	-ms-transform: scale(-1);
	transform: scale(-1);
}

.fl-node-<?php echo $id; ?> .pp-row-separator .pp-zigzag:before {
	background-image: -webkit-gradient(linear, 0 0, 300% 100%, color-stop(0.25, rgba(0, 0, 0, 0)), color-stop(0.25, #<?php echo $settings->separator_color; ?>));
	background-image: linear-gradient(315deg, #<?php echo $settings->separator_color; ?> 25%, rgba(0, 0, 0, 0) 25%), linear-gradient(45deg, #<?php echo $settings->separator_color; ?> 25%, rgba(0, 0, 0, 0) 25%);
	background-position: 50%;
	top: -90px;
}
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-pyramid:before {
	background-image: -webkit-gradient(linear, 0 0, 300% 100%, color-stop(0.25, rgba(0, 0, 0, 0)), color-stop(0.25, #<?php echo $settings->pyramid_color_1; ?>));
	background-image: linear-gradient(315deg, #<?php echo $settings->pyramid_color_1; ?> 25%, rgba(0, 0, 0, 0) 25%), linear-gradient(45deg, #<?php echo $settings->pyramid_color_2; ?> 25%, rgba(0, 0, 0, 0) 25%);
	background-position: 50%;
	top: -90px;
}
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-zigzag:after,
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-pyramid:after,
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-box-separator:after {
	height: 0;
}
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-box-separator:before {
	background-image: -webkit-gradient(linear, 100% 0, 0 100%, color-stop(0.5, #<?php echo $settings->separator_color; ?>), color-stop(0.5, #<?php echo $settings->separator_color; ?>));
	background-image: linear-gradient(to right, #<?php echo $settings->separator_color; ?> 50%, rgba(255, 255, 255, 0) 50%);
}
<?php } ?>

<?php if( $settings->separator_position == 'bottom' ) { ?>
.fl-node-<?php echo $id; ?> .pp-row-effects-wrap {
	bottom: 0;
	top: auto;
}
.fl-node-<?php echo $id; ?> .pp-row-separator svg {
	bottom: 0;
	top: auto;
	-webkit-transform: scaleY(-1);
	-moz-transform: scaleY(-1);
	-o-transform: scaleY(-1);
	-ms-transform: scaleY(-1);
	transform: scaleY(-1);
}
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-large-triangle-left {
	-webkit-transform: scaleY(1);
	-moz-transform: scaleY(1);
	-o-transform: scaleY(1);
	-ms-transform: scaleY(1);
	transform: scaleY(1);
}
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-tilt-right-separator {
	-webkit-transform: scaleX(-1);
	-moz-transform: scaleX(-1);
	-o-transform: scaleX(-1);
	-ms-transform: scaleX(-1);
	transform: scaleX(-1);
}
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-large-triangle-right {
	-webkit-transform: scaleX(-1);
	-moz-transform: scaleX(-1);
	-o-transform: scaleX(-1);
	-ms-transform: scaleX(-1);
	transform: scaleX(-1);
}
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-tilt-left-separator {
	-webkit-transform: scale(1);
	-moz-transform: scale(1);
	-o-transform: scale(1);
	-ms-transform: scale(1);
	transform: scale(1);
}
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-zigzag:after {
	background-image: -webkit-gradient(linear, 0 0, 300% 100%, color-stop(0.25, #<?php echo $settings->separator_color; ?>), color-stop(0.25, #<?php echo $settings->separator_color; ?>));
	background-image: linear-gradient(135deg, #<?php echo $settings->separator_color; ?> 25%, rgba(0, 0, 0, 0) 25%), linear-gradient(225deg, #<?php echo $settings->separator_color; ?> 25%, rgba(0, 0, 0, 0) 25%);
	background-position: 50%;
	top: 100%;
}
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-pyramid:after {
	background-image: -webkit-gradient(linear, 0 0, 300% 100%, color-stop(0.25, #<?php echo $settings->pyramid_color_1; ?>), color-stop(0.25, #<?php echo $settings->pyramid_color_2; ?>));
	background-image: linear-gradient(135deg, #<?php echo $settings->pyramid_color_1; ?> 25%, rgba(0, 0, 0, 0) 25%), linear-gradient(225deg, #<?php echo $settings->pyramid_color_2; ?> 25%, rgba(0, 0, 0, 0) 25%);
	background-position: 50%;
	top: 100%;
}
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-zigzag:before,
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-pyramid:before,
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-box-separator:before {
	height: 0;
}
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-box-separator:after {
	background-image: -webkit-gradient(linear, 100% 0, 0 100%, color-stop(0.5, #<?php echo $settings->separator_color; ?>), color-stop(0.5, #<?php echo $settings->separator_color; ?>));
	background-image: linear-gradient(to right, #<?php echo $settings->separator_color; ?> 50%, rgba(255, 255, 255, 0) 50%);
}
<?php } ?>

<?php if( $settings->separator_type == 'triangle_shadow' ) { ?>
	.fl-node-<?php echo $id; ?> .pp-row-separator .pp-large-triangle-shadow .pp-shadow-color {
		<?php if( $settings->separator_shadow_color ) { ?>fill: #<?php echo $settings->separator_shadow_color; ?>;<?php } ?>
	}
<?php } ?>

<?php if( $settings->separator_type == 'slit' ) { ?>
	.fl-node-<?php echo $id; ?> .pp-row-separator .pp-slit-separator .pp-slit-1 {
		<?php if( $settings->slit_1_color ) { ?>fill: #<?php echo $settings->slit_1_color; ?>;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .pp-row-separator .pp-slit-separator .pp-slit-2 {
		<?php if( $settings->slit_2_color ) { ?>fill: #<?php echo $settings->slit_2_color; ?>;<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .pp-row-separator .pp-slit-separator .pp-slit-3 {
		<?php if( $settings->slit_3_color ) { ?>fill: #<?php echo $settings->slit_3_color; ?>;<?php } ?>
	}
<?php } ?>

.fl-node-<?php echo $id; ?> .pp-row-separator svg {
	<?php if( $settings->separator_color ) { ?>fill: #<?php echo $settings->separator_color; ?>;<?php } ?>
	<?php if( $settings->separator_height ) { ?>height: <?php echo $settings->separator_height; ?>px;<?php } ?>
}
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-zigzag:before,
.fl-node-<?php echo $id; ?> .pp-row-separator .pp-pyramid:before {
	top: -<?php echo $settings->separator_height; ?>px;
}
