<?php

/**
 * @class PPRowEffectsModule
 */
class PPRowEffectsModule extends FLBuilderModule {

    /**
     * Constructor function for the module. You must pass the
     * name, description, dir and url in an array to the parent class.
     *
     * @method __construct
     */
    public function __construct()
    {
        parent::__construct(array(
            'name'          => __('Row Separator', 'bb-powerpack'),
            'description'   => __('Addon to modify the row.', 'bb-powerpack'),
            'category'		=> BB_POWERPACK_CAT,
            'dir'           => BB_POWERPACK_DIR . 'pp-row-effects/',
            'url'           => BB_POWERPACK_URL . 'pp-row-effects/',
            'editor_export' => true, // Defaults to true and can be omitted.
            'enabled'       => true, // Defaults to true and can be omitted.
            'partial_refresh'   => true
        ));

        /**
         * Use these methods to enqueue css and js already
         * registered or to register and enqueue your own.
         */
        // Already registered
		$this->add_css('font-awesome');
    }

    /**
     * Use this method to work with settings data before
     * it is saved. You must return the settings object.
     *
     * @method update
     * @param $settings {object}
     */
    public function update($settings)
    {
        return $settings;
    }

    /**
     * This method will be called by the builder
     * right before the module is deleted.
     *
     * @method delete
     */
    public function delete()
    {

    }

}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('PPRowEffectsModule', array(
	'general'      => array( // Tab
		'title'         => __('Separator', 'bb-powerpack'), // Tab title
		'sections'      => array( // Tab Sections
            'separator'      => array(
                'title'     => '',
                'fields'    => array(
                    'separator'     => array(
                        'type'  => 'select',
                        'label' => __('Show Separator', 'bb-powerpack'),
                        'default'   => 'no',
                        'options'   => array(
                            'no'    => __('No', 'bb-powerpack'),
                            'yes'    => __('Yes', 'bb-powerpack'),
                        ),
                        'toggle'    => array(
                            'yes'   => array(
                                'fields'    => array('separator_type')
                            )
                        )
                    ),
                    'separator_type'    => array(
                        'type'      => 'select',
                        'label'     => __('Type', 'bb-powerpack'),
                        'default'   => 'none',
                        'options'   => array(
                            'none'  => __('None', 'bb-powerpack'),
                            'triangle'  => __('Big Triangle', 'bb-powerpack'),
                            'triangle_shadow'  => __('Big Triangle with Shadow', 'bb-powerpack'),
                            'triangle_left'  => __('Big Triangle Left', 'bb-powerpack'),
                            'triangle_right'  => __('Big Triangle Right', 'bb-powerpack'),
                            'triangle_small'  => __('Small Triangle', 'bb-powerpack'),
                            'tilt_left'  => __('Tilt Left', 'bb-powerpack'),
                            'tilt_right'  => __('Tilt Right', 'bb-powerpack'),
                            'curve'  => __('Curve', 'bb-powerpack'),
                            'zigzag'    => __('ZigZag', 'bb-powerpack'),
                            'wave'    => __('Wave', 'bb-powerpack'),
                            'cloud'    => __('Cloud', 'bb-powerpack'),
                            'slit'    => __('Slit', 'bb-powerpack'),
                            'box'    => __('Boxes', 'bb-powerpack'),
                            'pyramid'    => __('Pyramid', 'bb-powerpack'),
                        ),
                        'toggle'    => array(
                            'triangle'   => array(
                                'fields'    => array('separator_color', 'separator_position', 'separator_height')
                            ),
                            'triangle_shadow'   => array(
                                'fields'    => array('separator_shadow_color', 'separator_color', 'separator_position', 'separator_height')
                            ),
                            'triangle_left'   => array(
                                'fields'    => array('separator_color', 'separator_position', 'separator_height')
                            ),
                            'triangle_right'   => array(
                                'fields'    => array('separator_color', 'separator_position', 'separator_height')
                            ),
                            'triangle_small'   => array(
                                'fields'    => array('separator_color', 'separator_position', 'separator_height')
                            ),
                            'tilt_left'   => array(
                                'fields'    => array('separator_color', 'separator_position', 'separator_height')
                            ),
                            'tilt_right'   => array(
                                'fields'    => array('separator_color', 'separator_position', 'separator_height')
                            ),
                            'curve'   => array(
                                'fields'    => array('separator_color', 'separator_position', 'separator_height')
                            ),
                            'zigzag'   => array(
                                'fields'    => array('separator_color', 'separator_position', 'separator_height')
                            ),
                            'wave'   => array(
                                'fields'    => array('separator_color', 'separator_position', 'separator_height')
                            ),
                            'cloud'   => array(
                                'fields'    => array('separator_color', 'separator_position', 'separator_height')
                            ),
                            'slit'   => array(
                                'fields'    => array('slit_1_color', 'slit_2_color', 'slit_3_color', 'separator_position', 'separator_height')
                            ),
                            'box'   => array(
                                'fields'    => array('separator_color', 'separator_position', 'separator_height')
                            ),
                            'pyramid'   => array(
                                'fields'    => array('pyramid_color_1', 'pyramid_color_2', 'separator_position', 'separator_height')
                            ),
                        ),
                    ),
                    'separator_color'   => array(
                        'type'      => 'color',
                        'label'     => __('Color', 'bb-powerpack'),
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'      => 'css',
                            'rules'     => array(
                                array(
                                    'selector'  => '.pp-row-separator svg',
                                    'property'  => 'fill'
                                ),
                                array(
                                    'selector'  => '.pp-row-separator .pp-large-triangle-shadow .pp-main-color',
                                    'property'  => 'fill'
                                ),
                            ),
                        ),
                    ),
                    'separator_shadow_color'   => array(
                        'type'      => 'color',
                        'label'     => __('Shadow Color', 'bb-powerpack'),
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'      => 'css',
                            'rules'     => array(
                                array(
                                    'selector'  => '.pp-row-separator .pp-large-triangle-shadow .pp-shadow-color',
                                    'property'  => 'fill'
                                ),
                            ),
                        ),
                    ),
                    'slit_1_color'   => array(
                        'type'      => 'color',
                        'label'     => __('Slit 1 Color', 'bb-powerpack'),
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'      => 'css',
                            'rules'     => array(
                                array(
                                    'selector'  => '.pp-row-separator .pp-slit-separator .pp-slit-1',
                                    'property'  => 'fill'
                                ),
                            ),
                        ),
                    ),
                    'slit_2_color'   => array(
                        'type'      => 'color',
                        'label'     => __('Slit 2 Color', 'bb-powerpack'),
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'      => 'css',
                            'rules'     => array(
                                array(
                                    'selector'  => '.pp-row-separator .pp-slit-separator .pp-slit-2',
                                    'property'  => 'fill'
                                ),
                            ),
                        ),
                    ),
                    'slit_3_color'   => array(
                        'type'      => 'color',
                        'label'     => __('Slit 3 Color', 'bb-powerpack'),
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'      => 'css',
                            'rules'     => array(
                                array(
                                    'selector'  => '.pp-row-separator .pp-slit-separator .pp-slit-3',
                                    'property'  => 'fill'
                                ),
                            ),
                        ),
                    ),
                    'pyramid_color_1'   => array(
                        'type'      => 'color',
                        'label'     => __('Color Left', 'bb-powerpack'),
                        'show_reset'    => true,
                    ),
                    'pyramid_color_2'   => array(
                        'type'      => 'color',
                        'label'     => __('Color Right', 'bb-powerpack'),
                        'show_reset'    => true,
                    ),
                    'separator_position'    => array(
                        'type'      => 'select',
                        'label'     => __('Position', 'bb-powerpack'),
                        'default'   => 'top',
                        'options'   => array(
                            'top'   => __('Top', 'bb-powerpack'),
                            'bottom'   => __('Bottom', 'bb-powerpack'),
                        )
                    ),
                    'separator_height'    => array(
                        'type'      => 'text',
                        'label'     => __('Height', 'bb-powerpack'),
                        'size'      => '5',
                        'maxlength'     => '3',
                        'default'       => '100',
                        'description'   => __('px', 'bb-powerpack'),
                        'preview'       => array(
                            'type'      => 'css',
                            'rules'     => array(
                                array(
                                    'selector'      => '.pp-row-separator svg',
                                    'property'      => 'height'
                                ),
                                array(
                                    'selector'      => '.pp-row-separator .pp-box-separator:before',
                                    'property'      => 'height'
                                ),
                                array(
                                    'selector'      => '.pp-row-separator .pp-box-separator:after',
                                    'property'      => 'height'
                                ),
                                array(
                                    'selector'      => '.pp-row-separator .pp-zigzag:after',
                                    'property'      => 'height'
                                ),
                                array(
                                    'selector'      => '.pp-row-separator .pp-zigzag:before',
                                    'property'      => 'height'
                                ),
                                array(
                                    'selector'  => '.pp-row-separator .pp-zigzag:before',
                                    'property'  => 'top'
                                ),
                            ),
                        ),
                    ),
                ),
            ),
		)
	),
));
