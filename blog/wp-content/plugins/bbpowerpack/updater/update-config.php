<?php

// this is the URL our updater / license checker pings. This should be the URL of the site with EDD installed
define( 'BEAVER_ADDONS_URL', 'https://wpbeaveraddons.com' ); // you should use your own CONSTANT name, and be sure to replace it throughout this file

// the name of your product. This should match the download name in EDD exactly
define( 'BEAVER_ADDONS_ITEM_NAME', 'PowerPack for Beaver Builder' ); // you should use your own CONSTANT name, and be sure to replace it throughout this file

if( !class_exists( 'EDD_SL_Plugin_Updater' ) ) {
	// load our custom updater
	include('EDD_SL_Plugin_Updater.php' );
}

function bb_powerpack_plugin_updater() {

	// retrieve our license key from the DB
	$license_key = trim( get_option( 'bb_powerpack_license_key' ) );

	// setup the updater
	$edd_updater = new EDD_SL_Plugin_Updater( BEAVER_ADDONS_URL, BB_POWERPACK_DIR . '/bb-powerpack.php', array(
			'version' 	=> '1.0.9', 				// current version number
			'license' 	=> $license_key, 		// license key (used get_option above to retrieve from DB)
			'item_name' => BEAVER_ADDONS_ITEM_NAME, 	// name of this plugin
			'author' 	=> 'Team IdeaBox - WP Beaver Addons'  // author of this plugin
		)
	);

}
add_action( 'admin_init', 'bb_powerpack_plugin_updater', 0 );

/************************************
* the code below is just a standard
* options page. Substitute with
* your own.
*************************************/

function bb_powerpack_license_menu() {
	$admin_label = get_option('ppwl_admin_label');
	$admin_label = trim($admin_label) != '' ? trim($admin_label) : _('PowerPack License');
	add_options_page( $admin_label, $admin_label, 'manage_options', 'pp-settings', 'bb_powerpack_license_page' );
}
add_action('admin_menu', 'bb_powerpack_license_menu');

function bb_powerpack_license_page() {
	$license 	= get_option( 'bb_powerpack_license_key' );
	$status 	= get_option( 'bb_powerpack_license_status' );
	?>
	<div class="wrap">
		<h2><?php
			$admin_label = get_option('ppwl_admin_label');
			$admin_label = trim($admin_label) != '' ? trim($admin_label) : _('PowerPack License');
			echo sprintf( __('%s Settings'), $admin_label );
		?></h2>
		<form method="post" action="options.php">
			<div class="icon32 icon32-pp-settings" id="icon-pp"><br /></div>
            <h2 class="nav-tab-wrapper wms-nav-tab-wrapper">
    			<?php
                    $current_tab = isset( $_GET['tab'] ) ? $_GET['tab'] : 'general';
    				echo '<a href="' . admin_url( 'admin.php?page=pp-settings&tab=general' ) . '" class="nav-tab ' . ( $current_tab == 'general' ? 'nav-tab-active' : '' ) . '">' . __( 'General' ) . '</a>';
					if ( ! get_option('ppwl_hide_form') || get_option('ppwl_hide_form') == 0 ) {
                    	echo '<a href="' . admin_url( 'admin.php?page=pp-settings&tab=white-label' ) . '" class="nav-tab ' . ( $current_tab == 'white-label' ? 'nav-tab-active' : '' ) . '">' . __( 'White Label' ) . '</a>';
					}
    			?>
    		</h2>
			<?php settings_fields('bb_powerpack_license'); ?>
			<?php if ( ! isset($_GET['tab']) || 'general' == $_GET['tab'] ) { ?>
			<p><?php echo sprintf(__('Enter your <a href="%s" target="_blank">license key</a> to enable remote updates and support.', 'bb-powerpack'), 'https://wpbeaveraddons.com/checkout/purchase-history/?utm_medium=powerpack&utm_source=license-settings-page&utm_campaign=license-key-link'); ?>
			<table class="form-table">
				<tbody>
					<tr valign="top">
						<th scope="row" valign="top">
							<?php _e('License Key'); ?>
						</th>
						<td>
							<input id="bb_powerpack_license_key" name="bb_powerpack_license_key" type="password" class="regular-text" value="<?php esc_attr_e( $license ); ?>" />
						</td>
					</tr>
					<?php if( false !== $license ) { ?>
						<tr valign="top">
							<th scope="row" valign="top">
								<?php _e('Activate License'); ?>
							</th>
							<td>
								<?php if( $status !== false && $status == 'valid' ) { ?>
									<span style="color: #267329; background: #caf1cb; padding: 5px 10px; text-shadow: none; border-radius: 3px; display: inline-block; text-transform: uppercase;"><?php _e('active'); ?></span>
									<?php wp_nonce_field( 'bb_powerpack_nonce', 'bb_powerpack_nonce' ); ?>
										<input type="submit" class="button-secondary" name="bb_powerpack_license_deactivate" value="<?php _e('Deactivate License'); ?>"/>
								<?php } else { ?>
									<span style="color: red; background: #ffcdcd; padding: 5px 10px; text-shadow: none; border-radius: 3px; display: inline-block; text-transform: uppercase;"><?php _e('invalid'); ?></span>
									<?php
									wp_nonce_field( 'bb_powerpack_nonce', 'bb_powerpack_nonce' ); ?>
									<input type="submit" class="button-secondary" name="bb_powerpack_license_activate" value="<?php _e('Activate License'); ?>"/>
								<?php } ?>
							</td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
			<?php submit_button(); ?>
			<?php } ?>
			<?php if ( ! get_option('ppwl_hide_form') || get_option('ppwl_hide_form') == 0 ) { ?>
				<?php if ( isset($_GET['tab']) && 'white-label' == $_GET['tab'] ) { ?>
					<table class="form-table">
						<tbody>
							<tr valign="top">
								<th scope="row" valign="top">
									<?php _e('Plugin Name'); ?>
								</th>
								<td>
									<input id="ppwl_plugin_name" name="ppwl_plugin_name" type="text" class="regular-text" value="<?php esc_attr_e( get_option('ppwl_plugin_name') ); ?>" />
								</td>
							</tr>
							<tr valign="top">
								<th scope="row" valign="top">
									<?php _e('Plugin Description'); ?>
								</th>
								<td>
									<textarea id="ppwl_plugin_desc" name="ppwl_plugin_desc" style="width: 25em;"><?php echo get_option('ppwl_plugin_desc'); ?></textarea>
								</td>
							</tr>
							<tr valign="top">
								<th scope="row" valign="top">
									<?php _e('Developer / Agency Name'); ?>
								</th>
								<td>
									<input id="ppwl_plugin_author" name="ppwl_plugin_author" type="text" class="regular-text" value="<?php echo get_option('ppwl_plugin_author'); ?>" />
								</td>
							</tr>
							<tr valign="top">
								<th scope="row" valign="top">
									<?php _e('Website URL'); ?>
								</th>
								<td>
									<input id="ppwl_plugin_uri" name="ppwl_plugin_uri" type="text" class="regular-text" value="<?php echo esc_url( get_option('ppwl_plugin_uri') ); ?>" />
								</td>
							</tr>
							<tr valign="top">
								<th scope="row" valign="top">
									<?php _e('Admin Label'); ?>
								</th>
								<td>
									<input id="ppwl_admin_label" name="ppwl_admin_label" type="text" class="regular-text" value="<?php echo get_option('ppwl_admin_label'); ?>" />
								</td>
							</tr>
							<tr valign="top">
								<th scope="row" valign="top">
									<?php _e('Builder Sidebar Label'); ?>
								</th>
								<td>
									<input id="ppwl_builder_label" name="ppwl_builder_label" type="text" class="regular-text" value="<?php echo get_option('ppwl_builder_label'); ?>" placeholder="PowerPack Modules" />
								</td>
							</tr>
							<tr valign="top">
								<th scope="row" valign="top">
									<?php _e( 'Hide white label settings' ); ?>
								</th>
								<td>
									<label for="ppwl_hide_form">
										<input id="ppwl_hide_form" name="ppwl_hide_form" type="checkbox" value="1" <?php echo get_option('ppwl_hide_form') == 1 ? 'checked="checked"' : '' ?> />
									</label>
								</td>
							</tr>
							<tr valign="top">
								<th scope="row" valign="top">
									<?php _e( 'Hide plugin' ); ?>
								</th>
								<td>
									<label for="ppwl_hide_plugin">
										<input id="ppwl_hide_plugin" name="ppwl_hide_plugin" type="checkbox" value="<?php echo absint(get_option('ppwl_hide_plugin')); ?>" <?php echo get_option('ppwl_hide_plugin') == 1 ? 'checked="checked"' : '' ?> />
									</label>
								</td>
							</tr>
						</tbody>
					</table>
					<script>
					jQuery(document).ready(function(){
						jQuery('#ppwl_hide_plugin').on('change', function() {
							if ( jQuery(this).is(':checked') ) {
								jQuery(this).val(1);
							} else {
								jQuery(this).val(0);
							}
						});
					});
					</script>
					<p style=""><?php _e( 'You can hide this form to prevent your client from seeing these settings.<br />To re-enable the form, you will need to first deactivate the plugin and activate it again.' ); ?></p>
					<?php submit_button(); ?>
				<?php } ?>
			<?php } else { ?>
				<?php if ( isset($_GET['tab']) && 'white-label' == $_GET['tab'] ) { ?>
				<p style=""><?php _e( 'Done! To re-enable the form, you will need to first deactivate the plugin and activate it again.' ); ?></p>
				<?php } ?>
			<?php } ?>
		</form>
		<hr />
		<h2><?php _e('Support', 'bb-powerpack'); ?></h2>
		<p><?php _e('For submitting any support queries, feedback, bug reports or feature requests, please visit <a href="https://wpbeaveraddons.com/contact/" target="_blank">this link</a>'); ?></p>
	<?php
}

function bb_powerpack_register_option() {
	// creates our settings in the options table
	if ( isset($_POST['bb_powerpack_license_key']) ) {
		register_setting('bb_powerpack_license', 'bb_powerpack_license_key', 'bb_powerpack_sanitize_license' );
	}
	if ( isset($_POST['ppwl_plugin_name']) ) {
		register_setting('bb_powerpack_license', 'ppwl_plugin_name', 'sanitize_text_field' );
		register_setting('bb_powerpack_license', 'ppwl_plugin_desc', 'esc_textarea' );
		register_setting('bb_powerpack_license', 'ppwl_plugin_author', 'sanitize_text_field' );
		register_setting('bb_powerpack_license', 'ppwl_plugin_uri', 'esc_url' );
		register_setting('bb_powerpack_license', 'ppwl_admin_label', 'sanitize_text_field' );
		register_setting('bb_powerpack_license', 'ppwl_builder_label', 'sanitize_text_field' );
		register_setting('bb_powerpack_license', 'ppwl_hide_form', 'intval' );
		register_setting('bb_powerpack_license', 'ppwl_hide_plugin', 'intval' );
	}
}
add_action('admin_init', 'bb_powerpack_register_option');

function bb_powerpack_sanitize_license( $new ) {
	$old = get_option( 'bb_powerpack_license_key' );
	if( $old && $old != $new ) {
		delete_option( 'bb_powerpack_license_status' ); // new license has been entered, so must reactivate
	}
	return $new;
}

/************************************
* this illustrates how to activate
* a license key
*************************************/

function bb_powerpack_activate_license() {

	// listen for our activate button to be clicked
	if( isset( $_POST['bb_powerpack_license_activate'] ) ) {

		// run a quick security check
	 	if( ! check_admin_referer( 'bb_powerpack_nonce', 'bb_powerpack_nonce' ) )
			return; // get out if we didn't click the Activate button

		// retrieve the license from the database
		$license = trim( get_option( 'bb_powerpack_license_key' ) );


		// data to send in our API request
		$api_params = array(
			'edd_action'=> 'activate_license',
			'license' 	=> $license,
			'item_name' => urlencode( BEAVER_ADDONS_ITEM_NAME ), // the name of our product in EDD
			'url'       => home_url()
		);

		// Call the custom API.
		$response = wp_remote_post( BEAVER_ADDONS_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

		// make sure the response came back okay
		if ( is_wp_error( $response ) )
			return false;

		// decode the license data
		$license_data = json_decode( wp_remote_retrieve_body( $response ) );

		// $license_data->license will be either "valid" or "invalid"

		update_option( 'bb_powerpack_license_status', $license_data->license );

	}
}
add_action('admin_init', 'bb_powerpack_activate_license');

/***********************************************
* Illustrates how to deactivate a license key.
* This will descrease the site count
***********************************************/

function bb_powerpack_deactivate_license() {

	// listen for our activate button to be clicked
	if( isset( $_POST['bb_powerpack_license_deactivate'] ) ) {

		// run a quick security check
	 	if( ! check_admin_referer( 'bb_powerpack_nonce', 'bb_powerpack_nonce' ) )
			return; // get out if we didn't click the Activate button

		// retrieve the license from the database
		$license = trim( get_option( 'bb_powerpack_license_key' ) );

		// data to send in our API request
		$api_params = array(
			'edd_action'=> 'deactivate_license',
			'license' 	=> $license,
			'item_name' => urlencode( BEAVER_ADDONS_ITEM_NAME ), // the name of our product in EDD
			'url'       => home_url()
		);

		// Call the custom API.
		$response = wp_remote_post( BEAVER_ADDONS_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

		// make sure the response came back okay
		if ( is_wp_error( $response ) )
			return false;

		// decode the license data
		$license_data = json_decode( wp_remote_retrieve_body( $response ) );

		// $license_data->license will be either "deactivated" or "failed"
		if( $license_data->license == 'deactivated' )
			delete_option( 'bb_powerpack_license_status' );

	}
}
add_action('admin_init', 'bb_powerpack_deactivate_license');

/************************************
* this illustrates how to check if
* a license key is still valid
* the updater does this for you,
* so this is only needed if you
* want to do something custom
*************************************/

function bb_powerpack_check_license() {

	global $wp_version;

	$license = trim( get_option( 'bb_powerpack_license_key' ) );

	$api_params = array(
		'edd_action' => 'check_license',
		'license' => $license,
		'item_name' => urlencode( BEAVER_ADDONS_ITEM_NAME ),
		'url'       => home_url()
	);

	// Call the custom API.
	$response = wp_remote_post( BEAVER_ADDONS_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

	if ( is_wp_error( $response ) )
		return false;

	$license_data = json_decode( wp_remote_retrieve_body( $response ) );

	if( $license_data->license == 'valid' ) {
		echo 'valid'; exit;
		// this license is still valid
	} else {
		echo 'invalid'; exit;
		// this license is no longer valid
	}
}

/**
 * White Labeling
 */
 function bb_powerpack_white_labeling( $all_plugins ) {

	$pp_plugin_path = plugin_basename(  BB_POWERPACK_DIR . '/bb-powerpack.php' );
	$all_plugins[$pp_plugin_path]['Name']           = get_option( 'ppwl_plugin_name' ) ? get_option( 'ppwl_plugin_name' ) : $all_plugins[$pp_plugin_path]['Name'];
	$all_plugins[$pp_plugin_path]['PluginURI']      = get_option( 'ppwl_plugin_uri' ) ? get_option( 'ppwl_plugin_uri' ) : $all_plugins[$pp_plugin_path]['PluginURI'];
	$all_plugins[$pp_plugin_path]['Description']    = get_option( 'ppwl_plugin_desc' ) ? get_option( 'ppwl_plugin_desc' ) : $all_plugins[$pp_plugin_path]['Description'];
	$all_plugins[$pp_plugin_path]['Author']         = get_option( 'ppwl_plugin_author' ) ? get_option( 'ppwl_plugin_author' ) : $all_plugins[$pp_plugin_path]['Author'];
	$all_plugins[$pp_plugin_path]['AuthorURI']      = get_option( 'ppwl_plugin_uri' ) ? get_option( 'ppwl_plugin_uri' )     : $all_plugins[$pp_plugin_path]['AuthorURI'];
	$all_plugins[$pp_plugin_path]['Title']          = get_option( 'ppwl_plugin_name' ) ? get_option( 'ppwl_plugin_name' ) : $all_plugins[$pp_plugin_path]['Title'];
	$all_plugins[$pp_plugin_path]['AuthorName']     = get_option( 'ppwl_plugin_author' ) ? get_option( 'ppwl_plugin_author' ) : $all_plugins[$pp_plugin_path]['AuthorName'];

	if ( get_option('ppwl_hide_plugin') == 1 ) {
	 unset($all_plugins[$pp_plugin_path]);
	}
	return $all_plugins;

 }
 add_filter( 'all_plugins', 'bb_powerpack_white_labeling', 10, 1 );
