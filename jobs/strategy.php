<?php require_once 'header.php'; ?>
<div class="container-standard" id="worker-dashboard-main">

    <div class="row" id="">
        <div class="col-xs-12 col-md-6 col-lg-3" id="team-progress-left-panel">

            <button>Start</button>
            <button>Pass</button>
            <hr>
            <div class="team-member-progress" id="">
                <img src="https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png" alt="">
                <div class="details" id="">
                    <h3>Rick James - <strong>Level A</strong></h3>
                    ██░░░░░░░░ 20%
                </div>
            </div>
            <div class="team-member-progress" id="">
                <img src="https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png" alt="">
                <div class="details" id="">
                    <h3>Jamie Ricks - <strong>Level A</strong></h3>
                    ██████░░ 80%
                </div>
            </div>
            <div class="team-member-progress" id="">
                <img src="https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png" alt="">
                <div class="details" id="">
                    <h3>Stan Salmon - <strong>Level A</strong></h3>
                    ███░░░░░ 45%
                </div>
            </div>
            <a href="#">Peer Review</a> - <a href="#">Referrals</a>
            <hr>


        </div>
        <div class="col-xs-12 col-md-6 col-lg-6" id="project-details-wrap">
            <div class="row" id="">
                <div class="col-xs-12 col-md-9" id="">
                    <div class="" id="details">
                        <p><strong>NEW PROJECT: </strong> Convert a .PSD into a Wordpress homepage.</p>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3" id="">
                    <div class="num-hours" id="">
                        1.5 HOURS
                    </div>
                </div>

            </div>
            <hr>
            <div class="row" id="getting-started">
                <div class="col-xs-12" id="">
                    <h2>GETTING STARTED</h2>
                    <br>
                    <div class="row" id="">
                        <div class="col-xs-1 checkbox" id="">
                            <input type="checkbox">
                        </div>
                        <div class="col-xs-8" id="">
                            Determine proper Photoshop page size and set guidelines
                        </div>
                        <div class="col-xs-3 icons" id="">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="row" id="">
                        <div class="col-xs-1 checkbox" id="">
                            <input type="checkbox">
                        </div>
                        <div class="col-xs-8" id="">
                            Download the PSD template
                        </div>
                        <div class="col-xs-3 icons" id="">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="row" id="">
                        <div class="col-xs-1 checkbox" id="">
                            <input type="checkbox">
                        </div>
                        <div class="col-xs-8" id="">
                            Do some work on the things that need to be worked on.
                        </div>
                        <div class="col-xs-3 icons" id="">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="row" id="">
                        <div class="col-xs-1 checkbox" id="">
                            <input type="checkbox">
                        </div>
                        <div class="col-xs-8" id="">
                            Upload the completed work to the server using the customer's credentials
                        </div>
                        <div class="col-xs-3 icons" id="">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="row" id="">
                        <div class="col-xs-1 checkbox" id="">
                            <input type="checkbox">
                        </div>
                        <div class="col-xs-8" id="">
                            FINISH TASK
                        </div>
                        <div class="col-xs-3 icons" id="">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            <i class="fa fa-info-circle" aria-hidden="true"></i>
                        </div>
                    </div>


                </div>
            </div>

        </div>
        <div class="col-xs-12 col-md-6 col-lg-3" id="dashboard-chat-panel-right">
            <div class="" id="chat-wrap">
                <div class="messsge-wrap" id="">
                    <img src="https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png" alt="">
                    <span>Hey mang, what tasks are you chilling on now?</span>
                </div>
                <div class="messsge-wrap message-right" id="">
                    <span>message</span>
                    <img src="https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png" alt="">
                </div>
            </div>
        </div>

    </div>

</div>
<hr>
<?php require_once 'footer.php'; ?>
