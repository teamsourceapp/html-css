<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
?>
<?php require_once 'common/db.php'; ?>
<?php require_once 'common/AmazonHelper.php'; ?>
<?php require_once 'header.php'; ?>

<?php
session_start();
if (count($_POST)) {
    foreach ($_POST as $key => $val) {
        $_SESSION['form_data'][$key] = $val;
    }
//    $applicantData = $_POST;
//    $applicantData['daysAvail'] = json_encode($_POST['daysAvail']);
//    $applicantData['timesAvail'] = json_encode($_POST['timesAvail']);
//    $applicantData['jobType'] = 'job_type';
//    $applicantData['resumeFileId'] = 'file_id';
//    if (count($_FILES) && isset($_FILES['resume_file']) && $_FILES['resume_file']['error'] == 0) {
//        $target = $_SERVER['DOCUMENT_ROOT'] . $_SERVER['REQUEST_URI'] . 'tmp/' . $_FILES['resume_file']['name'];
//        move_uploaded_file($_FILES["resume_file"]["tmp_name"], $target);
//        $applicantData['resumeFileId'] = AmazonHelper::uploadFileToAmazon($target);
//    }
//    $id = DbModel::model()->saveApplicant($applicantData);
    header('Location: step3.php');
    exit;
}
?>
<div class="container">
    <div class="row" id="">
        <div class="container col-md-5">
            <h2>Do you love your job?</h2>
            <p>If you’re a marketer, writer, designer or developer, we have everything you need to love freelancing.</p>
            <h4>You'll get work that works for you.</h4>
            <p>Lightning-fast payments are only the beginning. You’ll also be able to schedule the hours you’re available, giving you the flexibility to work when you’re at your best. </p>
            <h4>We'll find the work.</h4>
            <p>We take the pain out of freelancing by bringing you consistent work. You’ll spend your time on projects, not looking for clients.</p>
            <h4>You won’t be alone.</h4>
            <p>You’ll work with a group of colleagues whose skills complement your own. And our training and tools will help you polish your skills and grow as a professional.</p>
            <p>Submit your application today to get started.</p>
        </div>
        <div class="col-md-6 offset-md-1 container" id="dev-registration-form">
            <!--                        <h3>Register for Beta Access</h3>
                                    <hr>-->
            <form method="post" action="" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="enlishLevel">English level (1–10):</label>
                    <select class="form-control" id="enlishLevel" name="englishLevel" required>
                        <option value="-">—</option>
                        <?php
                        for ($i = 1; $i < 11; $i++)
                            echo "<option value='$i'>$i</option>"
                            ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleSelect2">Type of job I'm best at:</label>
                    <select class="form-control" id="exampleSelect2" name="bestJob" required>
                        <option value="-">—</option>
                        <option value="Marketing">Marketing</option>
                        <option value="Design">Design</option>
                        <option value="Writing">Writing</option>
                        <option value="Development">Development</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Other languages spoken</label>
                    <input type="text" name="otherLanguages" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                </div>
                <br>
                <h5>Work History<small> (if applicable)</small></h5>
                <hr>
                <div class="form-group">
                    <label for="linkedIn">LinkedIn profile URL</label>
                    <input type="text" class="form-control" name="linkedinURL" id="linkedIn" aria-describedby="linkedInHelp" placeholder="">
                </div>
                <div class="form-group">
                    <label for="">Upwork profile URL</label>
                    <input type="text" class="form-control" name="upworkURL" id="" aria-describedby="Help" placeholder="">
                </div>
                <div class="form-group">
                    <label for="">Onlinejobs.ph Profile URL</label>
                    <input type="text" class="form-control" name="onlinejobsphURL" id="" aria-describedby="Help" placeholder="">
                </div>
                <div class="form-group">
                    <label for="">Other URL?</label>
                    <input type="text" class="form-control" name="otherURL" id="" aria-describedby="Help" placeholder="">
                </div>
                <!--                <br>
                                <h5>Work Availability<small></small></h5>
                                <hr>
                                <div class="form-group">
                                    <label for="daysOfWeek">Days of the week available:</label>
                                    <select multiple class="form-control" id="daysOfWeek" name="daysAvail[]" required>
                                        <option value="Monday–Friday">Monday–Friday</option>
                                        <option value="All Week">All Week</option>
                                        <option disabled>────────────────────</option>
                                        <option value="Monday">Monday</option>
                                        <option value="Tuesday">Tuesday</option>
                                        <option value="Wednesday">Wednesday</option>
                                        <option value="Thursday">Thursday</option>
                                        <option value="Friday">Friday</option>
                                        <option value="Saturday">Saturday</option>
                                        <option value="Sunday">Sunday</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="timeOfDay">Times of the day available:</label>
                                    <select multiple class="form-control" id="timeOfDay" name="timesAvail[]" required>
                                        <option value="Work Hours">Work Hours (8a–5p)</option>
                                        <option value="All Day">All Day</option>
                                        <option value="All Night">All Night</option>
                                        <option disabled>────────────────────</option>
                                        <option value="Mornings">Mornings</option>
                                        <option value="Mid-day">Mid-day</option>
                                        <option value="Evenings">Evenings</option>
                                        <option value="Late night">Late night</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="numHoursAvail">Number of Hours Available</label>
                                    <input type="number" name="hoursAvail" class="form-control" id="numHoursAvail" aria-describedby="HelpnumHoursAvail" placeholder=""  required>
                                </div>
                                <h5>Resume</h5>
                                <hr>
                                <div class="form-group">
                                    <label for="exampleInputFile">Upload your resume/CV (if applicable)</label>
                                    <input type="file" class="form-control-file" name="resume_file" id="exampleInputFile" aria-describedby="fileHelp" required>
                                    <small id="fileHelp" class="form-text text-muted">This is some placeholder block-level help text for the above input. It's a bit lighter and easily wraps to a new line.</small>
                                </div>-->

                <button type="submit" class="btn btn-primary">Next</button>
            </form>
            <br/><br/><br/><br/>
        </div>
    </div>
</div>
<?php require_once 'footer.php'; ?>
