<?php

require_once dirname(__FILE__) . '/AmazonHelper.php';

class DbModel {

    private $host;
    private $db;
    private $user;
    private $pass;
    private $pdo;
    private static $instance;

    public static function model() {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct() {
        $params = require(dirname(__FILE__) . '/params.php');
        $this->host = $params['db_host'];
        $this->user = $params['db_user'];
        $this->pass = $params['db_password'];
        $this->db = $params['db_name'];
        $dsn = "mysql:host=$this->host;dbname=$this->db";
        try {
            $this->pdo = new PDO($dsn, $this->user, $this->pass);
        } catch (PDOException $e) {
            throw new pdoDbException($e);
        }
    }

    public function saveApplicant($data) {
        $fieldsList = array_keys($data);
        $fields = implode(',', $fieldsList);
        $values = implode(',:', $fieldsList);
        $query = 'INSERT INTO applicants(' . $fields . ') VALUES (:' . $values . ')';
        $stmt = $this->pdo->prepare($query);
        $params = array();
        foreach ($data as $field => $value) {
            $params[':' . $field] = $value;
        }
        try {
            $stmt->execute($params);
            return $this->pdo->lastInsertId();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

}
