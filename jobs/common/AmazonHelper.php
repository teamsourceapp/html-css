<?php

require_once dirname(__FILE__) . '/S3.php';

class AmazonHelper {

    public static function uploadFileToAmazon($targetFile, $path = '') {
        $params = require(dirname(__FILE__) . '/params.php');
        $s3_params = $params['s3'];
        $s3 = new S3($s3_params['access_key'], $s3_params['secret_key'], false, $s3_params['endpoint']);

        if (self::checkExsitingFileOnAmazon($path . baseName($targetFile))) {
            self::deleteFileFromAmazon($path . baseName($targetFile));
        }

        if ($s3->putObjectFile($targetFile, $s3_params['bucket_name'], $path . baseName($targetFile), S3::ACL_PUBLIC_READ)) {
            unlink($targetFile);
            return $path . baseName($targetFile);
        }
        return false;
    }

    private static function checkExsitingFileOnAmazon($name) {
        $params = require(dirname(__FILE__) . '/params.php');
        $s3_params = $params['s3'];
        $s3 = new S3($s3_params['access_key'], $s3_params['secret_key'], false, $s3_params['endpoint']);
        try {
            $info = $s3->getObjectInfo($s3_params['bucket_name'], $name, false);
            return $info;
        } catch (Exception $exc) {
            return false;
        }
    }

    private static function deleteFileFromAmazon($name, $path = '') {
        $params = require(dirname(__FILE__) . '/params.php');
        $s3_params = $params['s3'];
        $s3 = new S3($s3_params['access_key'], $s3_params['secret_key'], false, $s3_params['endpoint']);
        if ($s3->deleteObject($s3_params['bucket_name'], $path . $name)) {
            return true;
        }
        return false;
    }

}
