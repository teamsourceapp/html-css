<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
?>
<?php require_once 'common/db.php'; ?>
<?php require_once 'common/AmazonHelper.php'; ?>
<?php require_once 'header.php'; ?>

<?php
session_start();
if (count($_POST)) {
    $_SESSION['form_data'] = $_POST;
    header('Location: step2.php');
    exit;
}
?>
<div class="container">
    <div class="row" id="">
        <div class="container col-md-5">
            <h2>Do you love your job?</h2>
            <p>If you’re a marketer, writer, designer or developer, we have everything you need to love freelancing.</p>
            <h4>You'll get work that works for you.</h4>
            <p>Lightning-fast payments are only the beginning. You’ll also be able to schedule the hours you’re available, giving you the flexibility to work when you’re at your best. </p>
            <h4>We'll find the work.</h4>
            <p>We take the pain out of freelancing by bringing you consistent work. You’ll spend your time on projects, not looking for clients.</p>
            <h4>You won’t be alone.</h4>
            <p>You’ll work with a group of colleagues whose skills complement your own. And our training and tools will help you polish your skills and grow as a professional.</p>
            <p>Submit your application today to get started.</p>
        </div>
        <div class="col-md-6 offset-md-1 container" id="dev-registration-form">
            <h3>Register for Beta Access</h3>
            <hr>
            <form method="post" action="" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="firstName">First name</label>
                    <input type="text" class="form-control" id="firstName" name="firstName" aria-describedby="firstName" placeholder="" required>
                </div>
                <div class="form-group">
                    <label for="lastName">Last name</label>
                    <input type="text" class="form-control" id="lastName" name="lastName" aria-describedby="lastNameHelp" placeholder="" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" name="email" aria-describedby="emailHelp" placeholder="" required>
                </div>
                <div class="form-group">
                    <label for="city">City</label>
                    <input type="text" class="form-control" id="city" aria-describedby="cityHelp" name="city" placeholder="" required>
                </div>
                <div class="form-group">
                    <label for="country">Country</label>
                    <input type="text" class="form-control" id="country" name="country" aria-describedby="countryHelp" placeholder="" required>
                </div>
                <button type="submit" class="btn btn-primary">Next</button>
            </form>
            <br/><br/><br/><br/>
        </div>
    </div>
</div>
<?php require_once 'footer.php'; ?>
