PATH = '';
var categories = {
  1: "Design",
  2: "Marketing",
  3: "Development",
  4: "Content",
};
$(document).ready(function () {

  var activeChatId = null;
  var activeCustomerId = null;

  getTaskButtons();
  updateChat();
  
  // Import Task Buttons
  function getTaskButtons() {
    params = {
      "method":"allTSTasks",
      "params": null
    };
    console.log(params);
    $.ajax({type:"POST",url:PATH+"api.php",data:params,
      success: function(tasks) {
        console.log("Import Task buttons");
        console.log(tasks);

        // Iterate over chat boxes from DB and fill out the deetz
        _.each(tasks,function (task) {
          $('#task-buttons').append(
            '<div class="task '+categories[task.category_id]+'" data-id="'+task.id+'">'+task.slug+'</div>'
          )
        });

        // Click to add task to project
        $(".task").on("click", function(e){
          var task_id = $(this).attr('data-id');
          console.log(task_id);
          $('[chat-data-id="'+activeChatId+'"]').find('.list').append($(this).clone());
          addCustomerTask(task_id,activeCustomerId);
        });

      },
      error:   function(jqXHR, textStatus, errorThrown) {
        alert("Error, status = " + textStatus + ", " +
          "error thrown: " + errorThrown
        );
      }
    });
  }

  function addCustomerTask(task_id,activeCustomerId) {
    params = {
      "method":"addCustomerTask",
      "params": {
        "task_id": task_id,
        "customer_id": activeCustomerId
      }
    };
    console.log(params);
    $.ajax({type:"POST",url:PATH+"api.php",data:params,
      success: function(data) {
        console.log("Add Customer Task");
        console.log(data);
      },
      error:   function(jqXHR, textStatus, errorThrown) {
        alert("Error, status = " + textStatus + ", " +
          "error thrown: " + errorThrown
        );
      }
    });
  }
  
  // Update chat boxes
  function updateChat(){

    // get all chats that match my strategist ID
    params = {
      "method":"getStrategistChats",
      "params": null
    };
    console.log(params);
    $.ajax({type:"POST",url:PATH+"api.php",data:params,
      success: function(chats) {
        console.log("Get Strategist Chats");
        console.log(chats);

        // Iterate over chat boxes from DB and fill out the deetz
        $('.chat-window-active').remove();
        _.each(chats,function (chat) {
          console.log(chat);
          makeChatBox(chat);
          getChatMessages(chat);
          getChatTasks(chat);
        });

        $(".chat-window-active").on("click", function(e){
          console.log($(this).attr('chat-data-id'));
          $(".chat-window-active .chat.active").removeClass('active');
          $(this).find('.chat').addClass('active');
          activeChatId = $(this).attr('chat-data-id');
          activeCustomerId = $(this).attr('chat-customer-id');
        });

      },
      error:   function(jqXHR, textStatus, errorThrown) {
        alert("Error, status = " + textStatus + ", " +
          "error thrown: " + errorThrown
        );
      }
    });
  }

  function getChatMessages(chat) {
    _.each(chat.messages,function (message) {
      $('[chat-data-id="'+chat.id+'"]').find('.comments')
        .append('<comment>'+message.first_name+': '+message.message+'</comment>');
    })
  }

  function getChatTasks(chat) {
    console.log(chat);
    console.log(chat.customer_id);
    params = {
      "method":"getCustomerTasks",
      "params": {
        "customer_id": chat.customer_id
      }
    };
    console.log(params);
    $.ajax({type:"POST",url:PATH+"api.php",data:params,
      success: function(tasks) {
        console.log("Get Chat Tasks");
        console.log(tasks);
        _.each(tasks,function (task) {
          $('[chat-data-id="'+chat.id+'"]').find('.list')
          .append('<div class="task content" data-id="'+task.id+'">'+task.slug+'</div>');
        });
      },
      error:   function(jqXHR, textStatus, errorThrown) {
        alert("Error, status = " + textStatus + ", " +
          "error thrown: " + errorThrown
        );
      }
    });
  }

  function makeChatBox(chat) {
    $newChat = $('#chat-window-template').clone();
    $newChat.removeAttr('id')
      .attr('chat-data-id',chat.id)
      .attr('chat-customer-id',chat.customer_id)
      .addClass('chat-window-active');
    $newChat.find('header').html('<strong>'+chat.last_name+', '+chat.first_name+'</strong>'+' &nbsp;&nbsp;'+chat.email+' &nbsp;&nbsp;'+chat.phone+'');
    $('#chats-container').append($newChat);
    return chat.id;
  }

  // Submit New Customer
  $("#submit-new-customer").on("click", function(e){
    e.preventDefault();
    params = {
      "method":"createCustomer",
      "params": {
        "first_name": $('#first-name').val(),
        "last_name": $('#last-name').val(),
        "email": $('#email').val(),
        "phone": $('#phone').val()
      }
    };
    console.log(params);
    $.ajax({type:"POST",url:PATH+"api.php",data:params,
      success: function(data) {
        console.log("Create customer");
        console.log(data);
        updateChat();
      },
      error:   function(jqXHR, textStatus, errorThrown) {
        alert("Error, status = " + textStatus + ", " +
          "error thrown: " + errorThrown
        );
      }
    });




  });






  /*
   * WARNING: some boring utility functions below
   */

  // prevent default browser scroll when on top of a chat box
  $( '.scrollable' ).on( 'mousewheel DOMMouseScroll', function ( e ) {
    var e0 = e.originalEvent,
      delta = e0.wheelDelta || -e0.detail;

    this.scrollTop += ( delta < 0 ? 1 : -1 ) * 3;
    e.preventDefault();
  });

});