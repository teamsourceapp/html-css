
$(document).ready(function () {
  $(function(){$("#tabs").tabs();}); // start tabs
  $('#task-category').select2({width: '100%',placeholder: 'Select a category...'}); // start select2

});

/* Global utility functions - don't fuck with these */
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

(function($) {
  $.queryString = (function(a) {
    if (a == "") return {};
    var b = {};
    for (var i = 0; i < a.length; ++i)
    {
      var p=a[i].split('=');
      if (p.length != 2) continue;
      b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
    }
    return b;
  })(window.location.search.substr(1).split('&'))
})(jQuery);

function getTimeRemaining(endtime) {
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor((t / 1000) % 60);
  var minutes = Math.floor((t / 1000 / 60) % 60);
  var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
  var days = Math.floor(t / (1000 * 60 * 60 * 24));
  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}
function initializeClock(id, endtime, end) {
  var clock = document.getElementById(id);
  function updateClock() {
    var t = getTimeRemaining(endtime);
    clock.innerHTML = t.minutes +":"+('0' + t.seconds).slice(-2);
    if (t.total <= 0) {
      clearInterval(timeinterval);
      end();
    }
  }

  updateClock();
  var timeinterval = setInterval(updateClock, 1000);
}