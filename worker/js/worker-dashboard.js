/*
 * Created by Bryan Potts on Dec. 2nd, 2016
 * Worker Dashboard - version 0.3.1
 */
PATH = '';
var tasks = [
  {
    "id": 1,
    "title": "Convert a PSD to Web page",
    "description": "This task requires convertined a Photoshop file or similar mockup document and converting it into a Web site. Works should review any attached implementation notes specific to the project or the task. The finished product should be a page that is mobile responsive and free of //console errors, unless otherwise specificed.",
    "status": 0,
    "todos": [
      {
        "id": 1,
        "status": 1,
        "description": "Review all requirements, and confirm that all necessary assets have been provided.",
        "comments": [
          {
            "id": 1,
            "createdBy": 1,
            "dateCreate": "12/16/2016, 8:00pm EST",
            "comment": "The customer would like to use Pure Grid IO instead of Boostrap."
          },
          {
            "id": 2,
            "createdBy": 4,
            "dateCreate": "12/16/2016, 8:06pm EST",
            "comment": "The customer would like to use Pure Grid IO instead of Boostrap."
          }
        ],
        "assets": [
          {
            "id": 1,
            "name": "homepage-v1.psd"
          },
          {
            "id": 2,
            "name": "homepage-old.png"
          },
        ]
      },
      {
        "id": 2,
        "status": 0,
        "description": "Create raw HTML for all components using Boostrap",
        "comments": [
          {
            "id": 1,
            "createdBy": 1,
            "dateCreate": "12/16/2016, 8:00pm EST",
            "comment": "The customer would like to use Pure Grid IO instead of Boostrap."
          },
          {
            "id": 2,
            "createdBy": 4,
            "dateCreate": "12/16/2016, 8:06pm EST",
            "comment": "Okay, that won't be a problem."
          }
        ],
        "assets": [
          {
            "id": 1,
            "name": "homepage-v1.psd"
          },
          {
            "id": 2,
            "name": "homepage-old.png"
          },
        ]
      },
      {
        "id": 3,
        "status": 0,
        "description": "Check the finished page for syntax errors, //console errors, and possible code improvements or refactoring.",
        "comments": [
          {
            "id": 1,
            "createdBy": 1,
            "dateCreate": "12/16/2016, 8:00pm EST",
            "comment": "The customer would like to use Pure Grid IO instead of Boostrap."
          },
          {
            "id": 2,
            "createdBy": 4,
            "dateCreate": "12/16/2016, 8:06pm EST",
            "comment": "The customer would like to use Pure Grid IO instead of Boostrap."
          }
        ],
        "assets": [
          {
            "id": 1,
            "name": "homepage-v1.psd"
          },
          {
            "id": 2,
            "name": "homepage-old.png"
          },
        ]
      },
    ]
  }
];

var users = [
  {
    "id": 1,
    "username": "bpotts",
    "fname": "Bryan",
    "lname": "Potts",
    "rank": 7,
    "experience": 80,
    "pic": "https://randomuser.me/api/portraits/men/1.jpg",
    "activeTask": 1
  },
  {
    "id": 2,
    "username": "jayers",
    "fname": "John",
    "lname": "Ayers",
    "rank": 8,
    "experience": 30,
    "pic": "https://randomuser.me/api/portraits/men/2.jpg",
    "activeTask": 2
  }
];

// //console.log("==== Users ====");
// //console.log(users);
TASK_ID = null;
$(document).ready(function () {
  // Worker Dashboard
  (async function(){

    // Team Members
    async function addTeamMembers(){
      function addTeamMemberCard(user_id){
        user = users[user_id];
        $card = $('#team-member-card').clone();
        $card.removeAttr('id');
        $card.find('.name').html(user.fname+' '+user.lname);
        $card.find('.rank-label').html("TS–"+user.rank);
        $card.find('.rank-pill').addClass("rank-"+user.rank);
        $card.find('.progress .percent').html(user.experience+'%');
        $card.find('.pic-container img').attr("src",user.pic);
        // //console.log($card);
        $('#team-member-loading-card').before($card);
      }
      i=0; while(i<5){
        await sleep(Math.random()*1500); ////console.log('Adding new user');
        addTeamMemberCard(i);
        i++;
      }
      $('#team-member-loading-card').hide();
    }

    // Task and To-Do
    function initTask(task) {
      TASK_ID = task.id;
      // //console.log("==== SUGGESTED TASK ====");
      // //console.log(task);
      $('#task-title').html(task.new_title);
      $('#task-description').html(task.new_description);

      // To-Dos
      // //console.log("==== ToDos ====");
      var active_todo = false;
      _.each(task.todos,function(todo){
        //console.log(todo);

        // FIND ACTIVE TO-DO
        if(todo.status == 0 && active_todo !== true ){
          //console.log("inside active todo check");
          $('#active-to-do-item-description').html(todo.html);
          $('#active-todo').attr('data-id',todo.id);

          //console.log(todo.assets);

          // _.each(todo.assets, function(asset){
          //   //console.log(asset);
          //   $('#to-do-item-asset-links').append('<span><a href="#">'+asset.name+'</a></span>');
          // });

          // Comments
          // _.each(todo.comments,function(comment){
          //   $comment = $('#to-do-comment-template').clone();
          //   $comment.removeAttr('id');
          //   $comment.find('.pic img').attr("src",users[comment.createdBy].pic);
          //   $comment.find('.body').html(comment.comment);
          //   $('.to-do-comments').append($comment);
          // });
          // $('#active-to-do-num-comments').html("("+todo.comments.length+")");


          active_todo = true;

        // Display UNFINISHED Todos
        } else if(todo.status == 0) {
          $todo = $('#to-do-unfinished-template').clone();
          $todo.removeAttr('id');
          $todo.find('.body').html(todo.html);
          $('#unfinished-to-do-wrapper').append($todo);
          // _.each(todo.assets, function(asset){
          //   //console.log(asset);
          //   $todo.find('.assets').append('<span><a href="#">'+asset.name+'</a></span>');
          // });
        }

      })
    }


    /*
      Team Source Worker Dashboard - Get it Started in here, mang
     */
    console.log("==================== START WORKER DASHBOARD ======================");

    // GET NEXT TASK
    params = {
      "method":"getNextTask",
      "params": null
    };
    console.log(params);
    $.ajax({
      type:    "POST",
      url:     PATH + "api.php",
      data:    params,
      success: function(data) {
        console.log("Get Next Task - Success");
        console.log(data);
        initTask(data);
        var timeInMinutes = 3;
        var currentTime = Date.parse(new Date());
        var deadline = new Date(currentTime + timeInMinutes*60*1000);
        startAcceptTimer();
      },
      error:   function(jqXHR, textStatus, errorThrown) {
        alert("Error, status = " + textStatus + ", " +
          "error thrown: " + errorThrown
        );
      }
    });

    await sleep(1000);
    $('#start-task').show();
    $('#task-details-loading').hide(150);
    $('.task-tab').show(300);

    // Some events
    $('#to-do-active-comments-link').on("click",function(){
      $('.to-do-comments').show(150);
    });

    // CLICK START BUTTON
    function startTask(){

    }
    $("#start-task").on("click", function(e){

      params = {
        "method":"startTask",
        "params": {"task_id":TASK_ID}
      };
      console.log(params);
      $.ajax({type:"POST",url:PATH+"api.php",data:params,
        success: function(data) {
          console.log("Start button - Mark task as started");
          console.log(data);
          startTask();
          // alert("You have started a new task. Please keep this window open while you work.")
        },
        error:   function(jqXHR, textStatus, errorThrown) {
          alert("Error, status = " + textStatus + ", " +
            "error thrown: " + errorThrown
          );
        }
      });

      $('#not-started-warning').hide();
      $('#task-started-counter').show();
      $(this).hide();
      $('#finish-task').show();
      $('#team-member-loading-card').show();
      addTeamMembers();
    });

    // FINISH TASK - Top button just scrolls down... hehe
    $("#finish-task").on("click", function(e){
      $('html, body').animate({scrollTop: $(document).height()}, 'fast');
    });

    // ACTUALLY FINISH TASK
    $("#submit-task-button").on("click", function(e){
      e.preventDefault();
      params = {
        "method":"finishTask",
        "params": {
          "task_id":TASK_ID,
          "feedback": $('#feedback-content').val()
        }
      };
      console.log(params);
      $.ajax({type:"POST",url:PATH+"api.php",data:params,
        success: function(data) {
          console.log("Start button - Mark task as started");
          console.log(data);
          location.reload();
          alert("THANK YOU! Your task has been submitted for review. You may now start a new task.");
        },
        error:   function(jqXHR, textStatus, errorThrown) {
          alert("Error, status = " + textStatus + ", " +
            "error thrown: " + errorThrown
          );
        }
      });
    });

    $("#active-todo").change(function() {
      console.log("this checked",this.checked);

      // scroll comment section into view
      $('html, body').animate({
        scrollTop: $("#new-to-do-comment").offset().top - 100
      }, 1000);

      // IF CHECKED
      if(this.checked){
        console.log("checked");
        $('#new-to-do-comment-notice, #confirm-todo').show(200);
        $('#new-to-do-comment-question').hide(100);
      // IF NOT CHECKED
      } else {
        console.log("not checked");
        $('#new-to-do-comment-question').show(200);
        $('#new-to-do-comment-notice, #confirm-todo').hide(100);
      }
    });

    // CONFIRM BUTTON: Update the db with the new status, 1 or 0
    $("#confirm-todo").on("click", function(e){
      e.preventDefault();
      params = {
        "method":"updateTodoStatus",
        "params": {
          "customer_task_todo_id": $(this).attr('data-id'),
          "status": status
        }
      };
      console.log(params);
      $.ajax({type:"POST",url:PATH+"api.php",data:params,
        success: function(data) {
          console.log("Mark active todo as completed");
          console.log(data);
          location.reload();
          alert("THANK YOU! Your task has been submitted for review. You may now start a new task.");
        },
        error:   function(jqXHR, textStatus, errorThrown) {
          alert("Error, status = " + textStatus + ", " +
            "error thrown: " + errorThrown
          );
        }
      });
    });

  })();



});

function startAcceptTimer(){
  var timeInMinutes = 99;
  var currentTime = Date.parse(new Date());
  var deadline = new Date(currentTime + timeInMinutes*60*1000);
  var end = function () {
    alert("This task has expired. Your browser will now refresh.");
    location.reload();
  };
  initializeClock('accept-timer', deadline, end);
}

