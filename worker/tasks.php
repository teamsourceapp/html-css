<?php

require_once 'config.php';
$tasks = Task::getTasks();

require_once 'header.php';
?>

<div class="" id="tasks-page">
    <?php require_once 'tasks-subheader.php'; ?>
    <h1>All Teamsuorce Tasks</h1>
    <?php foreach($tasks as $task) { ?>
        <p>
            <a href="task-detail.php?id=<?=$task['id']?>"><?=$task['title']?> (<?=$task['category']?>)</a>
            <a href="edit-task.php?id=<?=$task['id']?>"> - edit</a>
        </p>
    <?php } ?>
</div>
<?php require_once 'footer.php'; ?>