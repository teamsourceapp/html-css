<?php
$_PAGE =  "worker-dashboard";
require_once 'boot.php';
require_once 'header.php';
?>

    <!-- Main JS -->
    <script src="js/worker-dashboard.js"></script>

<!-- Worker Dashboard -->
<div class="" id="worker-dash-body">

    <div class="pure-g">
        <div class="pure-u-1">

            <!-- Team + Task button -->
            <div class="pure-u-6-24" id="start-button-and-team">
                <button class="" id="start-task">START</button>
                <button class="" id="finish-task">FINISH</button>
                <hr class="no-margin">

                <!-- Team Member Card - Loading Placeholder -->
                <div class="pure-g team-member-card loading" id="team-member-loading-card">
                    <img src="http://pankajmalhotra.com/public/images/loading_dots.gif" alt="">
                </div> <!-- End Team Member Card -->

                <!-- Team Member Card -->
                <div class="pure-g team-member-card" id="team-member-card">
                    <div class="pic-container pure-u-1-5">
                        <img src="https://randomuser.me/api/portraits/men/83.jpg" alt="" class="user-pic">
                    </div>
                    <div class="pure-u-4-5 body">
                        <div class="name">
                            fname lnam
                        </div>
                        <div class="progress">
                            <div class="percent">progress</div>
                            <div class="bar"></div>
                            <br style="clear-after: both">
                        </div>
                        <div class="actions">
                            <a href="">profile</a>
                            <a href="">msg</a>
                        </div>
                        <div class="rank-pill">
                            <div class="rank-label">
                                RANK
                            </div>
                        </div>
                    </div>
                </div> <!-- End Team Member Card -->


            </div>

            <div class="pure-u-16-24" id="center-panel">

                <div class="" id="center-panel-header">
                    <h2>SUGGESTED TASK
                        <span id="not-started-warning">
                             – PLEASE ACCEPT OR SKIP
                            <span id="accept-timer">0:00</span>
                        </span>
                        <span id="task-started-counter"> – TASK STARTED (0:01)</span>
                    </h2>
                    <h1 id="task-title">[ task title ]</h1>
                </div>

                <div id="tabs">
                    <ul>
                        <li><a href="#tabs-1">Active Task</a></li>
                        <li><a href="#tabs-2">Project</a></li>
                        <li><a href="#tabs-3">Resources</a></li>
                        <li><a href="#tabs-4">Assets</a></li>
                    </ul>
                    <div id="tabs-1">
                        <img id="task-details-loading" src="http://pankajmalhotra.com/public/images/loading_dots.gif" alt="">
                        <div class="content task-tab">
                            <div class="task-details">
                                <p id="task-description">[ task description ]</p>
                            </div>
                            <hr class="after-details">
                            <div class="to-do-list">
                                <h1>To-Do List</h1>

                                <!-- Active TO-DO Item -->
                                <div class="fac fac-checkbox fac-default fac-info">
                                    <span></span>
                                    <input id="active-todo" type="checkbox" value="1">
                                    <label for="active-todo" data-id>
                                        <div class="content to-do-item">
                                            <div class="body" id="active-to-do-item-description">
                                                [ to-do item description ]
                                            </div>
                                            <div class="assets" id="to-do-item-asset-links">
                                                <!-- asset links here, inserted by JS -->
                                            </div>
                                            <div class="footer">
                                                <span id="to-do-active-comments-link"><a href="#">comments <small id="active-to-do-num-comments">(0)</small></a></span>
                                                <span class="skip-task-link"><a href="#">skip this task</a></span>
                                            </div>
                                            <div class="to-do-comments">
                                                <div class="to-do-comment" id="to-do-comment-template">
                                                    <div class="">
                                                        <div class="pic "><img src="" alt="" /></div>
                                                        <div class="body">[ comment body ]</div>
                                                        <br style="clear: both;">
                                                    </div>
                                                </div>
                                            </div>
                                            <p id="new-to-do-comment-notice">Please review your work and
                                                <strong>CONFIRM</strong> below. You may leave a comment with any notes on
                                                how to view or test your work, if needed.</p>
                                            <p id="new-to-do-comment-question">Do you have a question? Let us know by
                                                leaving a comment.</p>
                                            <textarea name="" id="new-to-do-comment" class="new-to-do-comment"
                                                      placeholder="Type a comment here..."></textarea>
                                            <button id="confirm-todo">CONFIRM</button>
                                        </div>
                                    </label>
                                </div>
                                <!-- END Active To-Do Item -->

                                <hr class="after-details">
                                <h2>Remaining To-Dos ([count])</h2>

                                <!-- UN-Finished ToDos -->
                                <div id="unfinished-to-do-wrapper">
                                    <div class="fac fac-checkbox-round fac-default fac-info off" id="to-do-unfinished-template">
                                        <span></span>
                                        <input id="box-example2" type="checkbox" value="1" disabled="disabled">
                                        <label for="box-example2">
                                            <div class="content to-do-item">
                                                <div class="body">
                                                    [ to-do item details ]
                                                </div>
                                                <div class="assets">
                                                    <!-- Assets, inserted via JS -->
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <!-- END UN-Finished ToDos -->


                            </div>
                            <hr class="after-details">
                            <div class="" id="feedback">
                                <h2>Finished? Please submit notes on how to view or test your work.</h2>
                                <textarea name="feedback" id="feedback-content" cols="30" rows="10" placeholder="Type message..."></textarea>
                                <button id="submit-task-button">Submit Task</button>
                                <br><br>
                            </div>
                        </div>
                    </div>
                    <div id="tabs-2">
                        <div class="content">
                            <p>Project Details show here...</p>
                        </div>
                    </div>
                    <div id="tabs-3">
                        <div class="content">
                            <p>Relevant resources pages show/link here...</p>
                        </div>
                    </div>
                    <div id="tabs-4" style="">
                        <div class="content">
                            <img src="https://seventhqueen.com/support/wp-content/uploads/2016/05/psd-file.png" alt="" style="width: 100px">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div> <!-- End worker dashboard -->

<div class="" id="chat-panel">

    <div class="" id="header">
        <h1>TeamSource Chat</h1>
        <div class="" id="chat-links">
            <span class="active">#task</span>
            <span>#project</span>
            <span>#team</span>
            <span>#general</span>
        </div>
    </div>
    <div class="" id="messages">

        <!-- message -->
        <div class="message">
            <div class="pic-container">
                <img src="https://randomuser.me/api/portraits/men/83.jpg" alt="" class="user-pic">
            </div>
            <div class="body">
                Hey what do you think of the Teamsource guys?
            </div>
        </div>
        <br style="clear: both;">

        <!-- message -->
        <div class="message mine">
            <div class="pic-container">
                <img src="https://randomuser.me/api/portraits/men/83.jpg" alt="" class="user-pic">
            </div>
            <div class="body">
                Very optimistic...
            </div>
        </div>
        <br style="clear: both;">
        <!-- message -->
        <div class="message">
            <div class="pic-container">
                <img src="https://randomuser.me/api/portraits/men/83.jpg" alt="" class="user-pic">
            </div>
            <div class="body">
                Yeah...
            </div>
        </div>
        <br style="clear: both;">

        <!-- message -->
        <div class="message mine">
            <div class="pic-container">
                <img src="https://randomuser.me/api/portraits/men/83.jpg" alt="" class="user-pic">
            </div>
            <div class="body">
                We use "..." a lot...
            </div>
        </div>
        <br style="clear: both;">

    </div>
    <br style="clear: both;">
    <div class="" id="new-message">
        <hr>
        <textarea name="" cols="30" rows="10" placeholder="Type a message..."></textarea>
        <!--<button>Send</button>-->
    </div>
</div>


<?php require_once 'footer.php'; ?>