<?php

require_once 'config.php';

$login_error = "";
if(isset($_POST['submit'])){
    $status = Task::addTask($_POST['task-title'],$_POST['task-category'],$_POST['task-body']);
    header("Location: tasks.php");
    die;
}

require_once 'header.php';
?>

<div class="" id="add-task-container">
    <form action="" method="post">
<!--    <h1>Search Tasks</h1> -->
<!--        <input type="text" name="task-search" placeholder="Search tasks by name or description..."><br>-->
<!--        <input type="submit" value="Search Tasks">-->
<!--        <hr>-->

        <?php require_once 'tasks-subheader.php'; ?>
        <h1>Add New Task</h1>
        <?=$login_error?>

        <input type="text" name="task-title" placeholder="Task Name" value="<?=$_POST['task-title']; ?>" required="required"><br>

        <select name="task-category" id="task-category" required="required" size="200">
            <option value="1">Marketing</option>
            <option value="2">Design</option>
            <option value="3">Development</option>
            <option value="4">Content</option>
            <option value="5">Teamsource</option>
        </select>
        <br><br>
        <textarea name="task-body" id="" cols="30" rows="10" placeholder="Task description..."><?=$_POST['task-body']?></textarea><br>

        <input type="hidden" name="submit" value="1">
        <input type="submit" value="Add Task">
    </form>
</div>
    <script>
        $(document).ready(function(){
            tinymce.init({ selector:'textarea' });
        })
    </script>
<?php require_once 'footer.php'; ?>