<?php require_once 'config.php';?>
<!doctype>
<html>
<head>
    <script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://unpkg.com/purecss@0.6.0/build/pure-min.css">
    <link rel="stylesheet" href="css/main.css?t=<?=time();?>">
    <link rel="stylesheet" href="css/misc.css">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://raw.githubusercontent.com/maxweldsouza/font-awesome-controls/master/bootstrap.min.css">

    <!-- Font Awesome -->
<!--    <script src="https://use.fontawesome.com/54589d8f03.js"></script>-->
    <link rel="stylesheet" href="css/fac.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

    <!-- jQuery UI -->
    <link rel="stylesheet" href="css/jquery-ui.min.css">
    <link rel="stylesheet" href="css/jquery-ui.structure.min.css">
    <script src="js/jquery-ui.min.js"></script>


    <!-- Misc Utils -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

    <!-- Select 2 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <!-- Main JS -->
    <script src="js/main.js"></script>

</head>
<body>

<!-- Header -->
<div class="">
    <div class="" id="worker-dash-header">
        <img src="images/logo.png" alt="" id="logo">
        <div class="" id="header-account">
            <?php if($_SESSION['user_logged_in']) { ?>
                <div class="name" id="">
                    <a href="index.php">Dashboard</a>
                    <a href="account.php">My Account</a>
                    <a href="logout.php">Sign Out</a>
                </div>
            <?php } else { ?>
                <a href="login.php">Sign In</a>
            <?php } ?>
        </div>
    </div>
</div>