<?php

require_once 'config.php';
$task = Task::getTaskDetail($_GET['id']);
$todos = Task::getTodos($_GET['id']);

require_once 'header.php';
?>

<div class="" id="tasks-page">

    <?php require_once 'tasks-subheader.php'; ?>
    <h1><?=$task['title']?> (<?=count($todos)?> Steps)</h1>
    <a href="edit-task.php?id=<?=$_GET['id']?>">Edit This Task</a>
    <hr>

    <?php foreach($todos as $todo) { ?>
        <!-- Active TO-DO Item -->
        <div class="fac fac-checkbox-round fac-default fac-info">
            <span></span>
            <input id="box-example-<?=$todo['id']?>" type="checkbox" value="1">
            <label for="box-example-<?=$todo['id']?>">
                <div class="content to-do-item">
                    <div class="body" id="">
                        <?=$todo['html']?>
                    </div>
                </div>
            </label>
        </div>
        <!-- END Active To-Do Item -->
    <?php } ?>
</div>

<?php require_once 'footer.php'; ?>