<?php

require_once 'config.php';

$login_error = "";
if(isset($_POST['submit'])){
    $user = User::login($_POST['email'],$_POST['password']);
    if($user){
        $_SESSION['user_logged_in'] = 1;
        $_SESSION['user'] = $user;
        User::loginRedirect($user);
    } else {
        $login_error = "<div id='login-error'><div>We could not find this account.</div><a href='reset.php'><small>password reset</small></a></div>";
    }
}

require_once 'header.php';
?>

<div class="" id="login-container">
    <form action="" method="post">
      <h1>Worker Login</h1>
        <p>Teamsource wants you to log in and work now.</p>
        <?=$login_error?>
        <input type="text" name="email" placeholder="Email" value="<?php if(isset($_POST['email'])) echo $_POST['email']; ?>"><br>
        <input type="password" name="password" placeholder="Password"><br>
        <input type="hidden" name="submit" value="1">
        <input type="submit" value="Login">
    </form>
</div>

<?php require_once 'footer.php'; ?>