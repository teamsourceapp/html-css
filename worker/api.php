<?php
$debug = 0;
session_start(); // use the session to pull current user deetz
//if($debug) print_r($_SESSION);
//if($debug) $_POST = ['method'=>'getNextTask','params'=>['worker_id'=>4]];
if($debug) $_POST = ['method'=>'addCustomerTask','params'=>['task_id'=>80]];
//if($debug) $_POST = ['method'=>'getTodos','params'=>['task_id'=>22]];
//if($debug) $_POST = ['method'=>'getCustomerTasks','params'=>['customer_id'=>77]];

if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['REQUEST_METHOD'] === 'POST' || $debug) {
    if(!$debug)header('Content-type: application/json');
    $api = new WorkerDash($_POST);
    echo $api->return;
    error_log(print_r($_POST,1));
    error_log("API output..."); error_log(print_r($api->return,1));
}

class WorkerDash {
    private $db;
    public $return;
    private $failed = ["status"=>"failed"];
    private $success = ["status"=>"success"];
    public function __construct($post) {
        //error_log(print_r($post,1));
        $method = $post['method'];
        $params = $post['params'];
        $this->db = mysqli_connect('localhost','root','root','teamsource');
        $r = json_encode($this->{$method}($params));
        $this->return = $r;
        //error_log("\nMessage Board API returning..."); error_log(print_r($r,1));
    }

    public function allTSTasks()
    {
        $sql = "select * from tasks where slug != '' order by category_id,title";
        if($stmt = $this->db->prepare($sql)) {
            $stmt->execute();
        }
        $r = $stmt->get_result();
        $R = $r->fetch_all(MYSQLI_ASSOC);
        return $R;
    }

    /*
     * Get the next task to show on the worker dashboard.
     * Use the php session to get the worker details
     */
    public function getNextTask($params)
    {
        $sql = "select ct.*, t.category_id
                from customer_tasks ct
                join tasks t on t.id = ct.task_id
                where t.category_id = ? and ct.status = 0;";
        if($stmt = $this->db->prepare($sql)) {
            error_log("session user id: " . $_SESSION['user']['id']);
            $stmt->bind_param('i', $_SESSION['user']['id']);
            $stmt->execute();
        }
        $r = $stmt->get_result();
        $R = $r->fetch_array(MYSQLI_ASSOC);
        if($R){
            $R['todos'] = $this->getCustomerTodos(['task_id'=>$R['task_id']]);
            return $R;
        } else {
            error_log("getNextTask failed");
        }
        return $this->failed;
    }

    public function getCustomerTodos($params)
    {
        $sql = "select ctt.*,tt.html from customer_task_todos ctt
                left join task_todos tt on tt.id = ctt.todo_id
                where ctt.task_id = ?";
        error_log('task id');
        error_log($params['task_id']);
        if($stmt = $this->db->prepare($sql)) {
            $stmt->bind_param('i', $params['task_id']);
            $stmt->execute();
        }
        $r = $stmt->get_result();
        $R = $r->fetch_all(MYSQLI_ASSOC);
        //foreach($R as $k => $v) $R[$k] = (object) $v;
        return $R;
    }

    public function getCustomerTasks($params)
    {
        $sql = "select ct.*,t.slug from customer_tasks ct
                join tasks t on t.id = ct.task_id
                where ct.customer_id = ? order by ct.id;";
//        echo $sql;
//        print_r($params);
//        echo $params['customer_id'];
//        die;
        if($stmt = $this->db->prepare($sql)) {
            $stmt->bind_param('i', $params['customer_id']);
            $stmt->execute();
        }
        $r = $stmt->get_result();
        $R = $r->fetch_all(MYSQLI_ASSOC);
        return $R;
    }

    public function startTask($params)
    {
        $sql = "update customer_tasks set worker_id = ?, status = 1 where id = ?";
        $stmt = $this->db->prepare($sql);
        $stmt->bind_param("ii",$_SESSION['user']['id'],$params['task_id']);
        $stmt->execute();
        return $this->success;
    }

    public function finishTask($params)
    {
        $sql = "update customer_tasks set
                worker_id = ?,
                status = 2,
                finish_notes = ?
                where id = ?";
        $stmt = $this->db->prepare($sql);
        $stmt->bind_param("isi",$_SESSION['user']['id'],$params['feedback'],$params['task_id']);
        $stmt->execute();
        return $this->success;
    }

    public function getTodosTemplate($task_id)
    {
        $sql = "select * from task_todos where task_id = ?";
        if($stmt = $this->db->prepare($sql)) {
            $stmt->bind_param('i', $task_id);
            $stmt->execute();
        }
        $r = $stmt->get_result();
        if($R = $r->fetch_all(MYSQLI_ASSOC)){
            return $R;
        }
        return $this->failed;
    }

    public function updateTodoStatus($params)
    {
        $sql = "update customer_task_todos set worker_id = ?, status = ? where id = ?";
        $stmt = $this->db->prepare($sql);
        $stmt->bind_param("iii",$_SESSION['user']['id'],$params['status'],$params['customer_task_todo_id']);
        $stmt->execute();
        if($stmt->affected_rows > 0){
            return $this->success;
        }
        return $this->failed;
    }

    public function getTask($task_id)
    {
        $sql = "select * from tasks where id = ?";
        if($stmt = $this->db->prepare($sql)) {
            $stmt->bind_param('i', $task_id);
            $stmt->execute();
        }
        $r = $stmt->get_result();
        if($R = $r->fetch_assoc()){
            return $R;
        }
        return $this->failed;
    }

    public function copyCustomerTaskTodos($todos,$params,$new_customer_task_id)
    {
        foreach($todos as $k => $todo){
            $sql = "insert into customer_task_todos
                (task_id,todo_id,customer_task_id,order_num,strategist_id,created_by)
                VALUES (?,?,?,?,?,?);";
            $stmt = $this->db->prepare($sql);
            $order = $k;
            $stmt->bind_param(
                "iiiiii",
                $todo['task_id'],
                $todo['id'],
                $new_customer_task_id,
                $k,
                $_SESSION['user']['id'],
                $_SESSION['user']['id']
            );
            $stmt->execute();
        }
        $new_customer_task_todo_id = mysqli_insert_id($this->db);
        return $new_customer_task_todo_id ? $new_customer_task_todo_id : $this->failed;
    }

    public function addCustomerTask($params)
    {
        $template = $this->getTask($params['task_id']);
        $todosTemplate = $this->getTodosTemplate($params['task_id']);
        error_log("template:");
        error_log(print_r($todosTemplate,1));
        $sql = "insert into customer_tasks
                (customer_id,task_id,new_title,strategist_id,created_by)
                VALUES (?,?,?,?,?);";
        if($stmt = $this->db->prepare($sql)) {
            $stmt->bind_param(
                "iisii",
                $params['customer_id'],
                $params['task_id'],
                $template['title'],
                $_SESSION['user']['id'],
                $_SESSION['user']['id']
            );
            $stmt->execute();
//            print_r($stmt); die;
        } else {
            error_log("db error:" . print_r($this->db->error,1));
            return (object) $this->failed;
        }
        $new_customer_task_id = mysqli_insert_id($this->db);

        $this->copyCustomerTaskTodos($todosTemplate,$params,$new_customer_task_id);

        error_log("\$new_customer_task_id");
        error_log($new_customer_task_id);
        return $new_customer_task_id ? $new_customer_task_id : $this->failed;
    }

    public function createCustomer($params)
    {
        $password = 'DONKEYappleRED';
        $type = 2;
        $sql = "insert into users
                (first_name,last_name,email,password,phone,`type`,created_by)
                VALUES (?,?,?,?,?,?,?);";
        $stmt = $this->db->prepare($sql);
        $stmt->bind_param(
            "sssssii",
            $params['first_name'],
            $params['last_name'],
            $params['email'],
            $password,
            $params['phone'],
            $type,
            $_SESSION['user']['id']
        );
        $stmt->execute();
        $new_customer_id = mysqli_insert_id($this->db);
        if($new_customer_id){
            $this->createProject($new_customer_id);
        }
        return $new_customer_id ? $this->getCustomer($new_customer_id) : $this->failed;
    }

    public function getCustomer($customer_id)
    {
        $sql = "select * from users where id = ?";
        if($stmt = $this->db->prepare($sql)) {
            $stmt->bind_param('i', $customer_id);
            $stmt->execute();
        }
        $r = $stmt->get_result();
        if($R = $r->fetch_assoc()){
            return $R;
        }
        return $this->failed;
    }

    public function getProject($project_id)
    {
        $sql = "select * from projects where id = ?";
        if($stmt = $this->db->prepare($sql)) {
            $stmt->bind_param('i', $project_id);
            $stmt->execute();
        }
        $r = $stmt->get_result();
        if($R = $r->fetch_assoc()){
            return $R;
        }
        return $this->failed;
    }

    public function createProject($customer_id)
    {
        $sql = "insert into projects
                (customer_id,strategist_id,created_by)
                VALUES (?,?,?);";
        $stmt = $this->db->prepare($sql);
        $stmt->bind_param(
            "iii",
            $customer_id,
            $_SESSION['user']['id'],
            $_SESSION['user']['id']
        );
        $stmt->execute();
        $new_project_id = mysqli_insert_id($this->db);
        if($new_project_id){
            $this->createChat($new_project_id);
        }
        return $new_project_id ? $this->getProject($new_project_id) : $this->failed;
    }

    public function getChat($chat_id)
    {
        $sql = "select * from chats where id = ?";
        if($stmt = $this->db->prepare($sql)) {
            $stmt->bind_param('i', $project_id);
            $stmt->execute();
        }
        $r = $stmt->get_result();
        if($R = $r->fetch_assoc()){
            return $R;
        }
        return $this->failed;
    }

    public function createChat($project_id)
    {
        $sql = "insert into chats
                (project_id,created_by)
                VALUES (?,?);";
        if($stmt = $this->db->prepare($sql)) {
            $stmt->bind_param(
                "ii",
                $project_id,
                $_SESSION['user']['id']
            );
            $stmt->execute();
        } else {
            error_log("db error:" . print_r($this->db->error,1));
            return (object) $this->failed;
        }
        $new_chat_id = mysqli_insert_id($this->db);
        if($new_chat_id){
            $new_message_id = $this->saveMessage($new_chat_id,0,"Project ID <strong>$project_id</strong> was created.",1);
        }
        return $new_chat_id ? $this->getChat($new_chat_id) : $this->failed;
    }

    public function saveMessage($chat_id,$user_id,$message,$is_auto)
    {
        $sql = "insert into messages
                (user_id,chat_id,is_auto,message,created_by)
                VALUES (?,?,?,?,?);";
        if($stmt = $this->db->prepare($sql)) {
            error_log($sql);
            $stmt->bind_param(
                "iiisi",
                $user_id,
                $chat_id,
                $is_auto,
                $message,
                $_SESSION['user']['id']
            );
            $stmt->execute();
        } else {
            error_log("db error:" . print_r($this->db->error,1));
            return (object) $this->failed;
        }
        $new_message_id = mysqli_insert_id($this->db);
        return $new_message_id ? $new_message_id : $this->failed;
    }

    public function getStrategistChats($strategist_id=false)
    {
        if( ! $strategist_id) $strategist_id = $_SESSION['user']['id'];

        error_log("\$strategist_id");
        error_log($strategist_id);
        $sql = "select c.id,first_name,last_name,email,phone,p.customer_id from projects p
                left join chats c on p.id = c.project_id
                join users u on u.id = p.customer_id
                where p.strategist_id = ?;";
        if($stmt = $this->db->prepare($sql)) {
            $stmt->bind_param('i', $strategist_id);
            $stmt->execute();
        }
        $r = $stmt->get_result();
        $R = $r->fetch_all(MYSQLI_ASSOC);
        error_log("getStrategistChats results");
        error_log(print_r($R,1));
        foreach($R as $k => $chat){
            $R[$k]['messages'] = $this->getChatMessages($chat['id']);
        }
        return $R;
    }

    public function getChatMessages($chat_id)
    {
        $sql = "select m.*,first_name,last_name from messages m
                join users u on u.id = m.user_id
                where chat_id = ?;";
        if($stmt = $this->db->prepare($sql)) {
            $stmt->bind_param('i', $chat_id);
            $stmt->execute();
        }
        $r = $stmt->get_result();
        $R = $r->fetch_all(MYSQLI_ASSOC);
        error_log("getChatMessages results");
        error_log(print_r($R,1));
        return $R;
    }

    public function getChatTasks($chat_id)
    {
        $sql = "select m.*,first_name,last_name from messages m
                join users u on u.id = m.user_id
                where chat_id = ?;";
        if($stmt = $this->db->prepare($sql)) {
            $stmt->bind_param('i', $chat_id);
            $stmt->execute();
        }
        $r = $stmt->get_result();
        $R = $r->fetch_all(MYSQLI_ASSOC);
        error_log("getChatMessages results");
        error_log(print_r($R,1));
        return $R;
    }
}
