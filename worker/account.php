<?php

require_once 'config.php';

$login_error = "";
if(isset($_POST['submit'])){
    $login_attempt = User::login($_POST['email'],$_POST['password']);
    if($login_attempt){
        $_SESSION['user_logged_in'] = 1;
        header("Location: dashboard.php");
    } else {
        $login_error = "<div id='login-error'><div>We could not find this account.</div><a href='reset.php'><small>password reset</small></a></div>";
    }
}

require_once 'header.php';
?>

<div class="" id="login-container">
    <form action="" method="post">
        <h1>My Account</h1>
        <?=$login_error?>
        <p>First Name</p>
        <input type="text" name="email" placeholder="First Name" value="<?php if(isset($_POST['email'])) echo $_POST['email']; ?>"><br>
        <p>Last Name</p>
        <input type="password" name="password" placeholder="Last Name"><br>

        <p>Email</p>
        <input type="password" name="password" placeholder="Email"><br>

        <input type="hidden" name="submit" value="1">
        <input type="submit" value="Login">
    </form>
</div>

<?php require_once 'footer.php'; ?>