<?php

class User {
    public static function login($email,$password)
    {
        global $conn;
        $sql = "select * from users where email = ? and password = ? LIMIT 1";
        if($stmt = $conn->prepare($sql)){
            $stmt->bind_param("ss", $email,$password);
            $res = $stmt->execute();
            $r = $stmt->get_result();
            $row = $r->fetch_array(MYSQLI_ASSOC);
        }
        return isset($row['id']) ? $row : 0;
    }

//    public static function register($params)
//    {
//        $sql = "insert into users
//                (type,email,password,first_name,last_name)
//                VALUES (3,?,?,?,?);";
//        $stmt = $this->db->prepare($sql);
//        $stmt->bind_param(
//            "iii",
//            $customer_id,
//            $_SESSION['user']['id'],
//            $_SESSION['user']['id']
//        );
//        $stmt->execute();
//        $new_project_id = mysqli_insert_id($this->db);
//        if($new_project_id){
//            $this->createChat($new_project_id);
//        }
//        return $new_project_id ? $this->getProject($new_project_id) : $this->failed;
//    }

    public static function loginRedirect($user)
    {
        if($user['type']==1){
            header("Location: strategy.php");
        } elseif($user['type']==2){
            header("Location: customer.php");
        } elseif($user['type']==3){
            header("Location: index.php");
        }

    }
}

class Task {

    public static function addTask($title,$catetory_id,$body) {
        global $conn;
        $sql = "insert into tasks (category_id,title) VALUES (?,?)";
        error_log("insert new topic: " . $sql);
        if($stmt = $conn->prepare($sql)) {
            $stmt->bind_param("is", $catetory_id, $title);
            $r = $stmt->execute();
            if ($r === false) {
                trigger_error('Statement execute failed! ' . htmlspecialchars(mysqli_stmt_error($stmt)), E_USER_ERROR);
            }
            $new_task_id = mysqli_insert_id($conn);
        } else {
            error_log("db error:" . print_r($conn->error,1));
            return false;
        }
        self::extractTodos($new_task_id,$body);
        return $new_task_id ? $new_task_id : false;

    }

    public static function updateTask($id,$title,$category_id,$body)
    {
        global $conn;
        self::deleteTodos($id);
        $sql = "update tasks SET title = ?, category_id = ? where id = ?";
        error_log("update task: " . $sql);
        if($stmt = $conn->prepare($sql)) {
            $stmt->bind_param("sii", $title,$category_id,$id);
            $r = $stmt->execute();
            if ($r === false) {
                trigger_error('Statement execute failed! ' . htmlspecialchars(mysqli_stmt_error($stmt)), E_USER_ERROR);
            }
        } else {
            error_log("db error:" . print_r($conn->error,1));
            return false;
        }
        self::extractTodos($id,$body);
        return $id;
    }

    public static function deleteTodos($task_id)
    {
        global $conn;
        $sql = "delete from task_todos where task_id = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("i",$task_id);
        $stmt->execute();
        return true;
    }

    public static function extractTodos($task_id,$content) {
        global $conn;
        $todos = explode('[x]',$content);
        array_shift($todos);
        $todo_ids = [];
        foreach($todos as $order => $todo){
            $sql = "insert into task_todos (task_id,order_num,html) VALUES (?,?,?)";
            error_log("insert new topic: " . $sql);
            if($stmt = $conn->prepare($sql)) {
                $stmt->bind_param("iis", $task_id, $order, $todo);
                $r = $stmt->execute();
                if ($r === false) {
                    trigger_error('Statement execute failed! ' . htmlspecialchars(mysqli_stmt_error($stmt)), E_USER_ERROR);
                }
                $todo_ids[] = mysqli_insert_id($conn);
            } else {
                error_log("db error:" . print_r($conn->error,1));
                return false;
            }
        }
        return $todo_ids;
    }

    public static function getTasks()
    {
        global $conn;
        $sql = "select t.*,c.name as category, c.id as category_id from tasks t left join categories c on t.category_id = c.id order by title;";
        $res = $conn->query($sql);
        $rows = $res->fetch_all(MYSQLI_ASSOC);
        return $rows;
    }

    public static function getTaskDetail($task_id)
    {
        global $conn;
        $sql = "select t.*,c.name as category from tasks t left join categories c on t.category_id = c.id where t.id = $task_id;";
        error_log($sql);
        $res = $conn->query($sql);
        $rows = $res->fetch_assoc();
        return $rows;
    }

    public static function getTodos($task_id)
    {
        global $conn;
        $sql = "select * from task_todos where task_id = $task_id order by order_num;";
        $res = $conn->query($sql);
        $rows = $res->fetch_all(MYSQLI_ASSOC);
        return $rows;
    }

    public static function unextractTodos($todos)
    {
        $body = "";
        foreach($todos as $todo){
            $body .= "[x]".$todo['html'];
        }
        return $body;
    }
}