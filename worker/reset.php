<?php

require_once 'config.php';

$login_error = "";
if(isset($_POST['submit'])){
    $login_attempt = User::login($_POST['email'],$_POST['password']);
    if($login_attempt){
        $_SESSION['user_logged_in'] = 1;
        header("Location: dashboard.php");
    } else {
        $login_error = "<div id='login-error'><div>We could not find your email.</div><a href='#'><small>contact support</small></a></div>";
    }
}

require_once 'header.php';
?>

<div class="" id="login-container">
    <form action="" method="post">
    <h1>Password Reset</h1>
    <?=$login_error?>
    <input type="text" name="email" placeholder="Email" value="<?php if(isset($_POST['email'])) echo $_POST['email']; ?>"><br>
    <input type="hidden" name="submit" value="1">
    <input type="submit" value="Send Reset Email">
    </form>
</div>

<?php require_once 'footer.php'; ?>