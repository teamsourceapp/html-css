<!doctype>
<html>
<head>
    <script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://raw.githubusercontent.com/maxweldsouza/font-awesome-controls/master/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- jQuery UI -->
    <link rel="stylesheet" href="css/jquery-ui.min.css">
    <link rel="stylesheet" href="css/jquery-ui.structure.min.css">
    <script src="js/jquery-ui.min.js"></script>

    <!-- Main JS -->
<!--    <script src="js/main.js"></script>-->
    <script src="js/strategy.js"></script>

    <!-- Misc Utils -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>

    <link rel="stylesheet" href="https://unpkg.com/purecss@0.6.0/build/pure-min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/misc.css">

</head>
<body>

<div class="" id="war-header">
    <strong style="margin-left: 5px">TEAMSOURCE STRATEGY - WAR ROOM</strong>
    <a href="logout.php">LOGOUT</a>
</div>

<div class="pure-g" id="war-container">

    <div class="pure-u-1-5" id="grab-panel">

        <div class="" id="grab-panel-search">
            <input type="text" placeholder="Search by name or description">
        </div>

        <div class="section">
            <div class="tasks" id="task-buttons">
                <!-- tasks buttons are generated here -->
                <div class="task marketing" data-id="1">CUSTOM</div>
                <div class="task design" id>PPC</div>
                <div class="task content" id>Lead Generation</div>
                <div class="task development" id>Video</div>
                <div class="task other" id>Retargeting</div>
            </div>
            <br style="clear: both;">
        </div>

    </div>
    <div class="chats pure-u-4-5" id="">
        <div class="pure-g" id="chats-container">

            <!-- Add New Customer -->
            <div class="pure-u-12-24">
                <div class="chat new-customer active">
                    <h1>Add New Customer</h1>
                    <input type="text" id="first-name" placeholder="First Name">
                    <input type="text" id="last-name" placeholder="Last Name">
                    <input type="text" id="email" placeholder="Email">
                    <input type="text" id="phone" placeholder="Mobile #">
                    <button id="submit-new-customer">Create</button>
                </div>
            </div>

            <!-- CHAT WINDOW TEMPLATE -->
            <div class="pure-u-12-24 chat-window" id="chat-window-template">
                <div class="chat pure-g">
                    <div class="pure-u-1-5 list">
                        <!-- tasks populated here -->
                    </div>
                    <div class="pure-u-4-5 chat-body">
                        <header><strong>[last_name, [first_name]</strong> [email]</header>
                        <div class="comments scrollable">
                            <!-- <comments> go here -->
                        </div>
                        <footer><input type="text" placeholder="Type your message..."></footer>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>


</body>
</html>
