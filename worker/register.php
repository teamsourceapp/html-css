<?php

require_once 'config.php';

$login_error = "";
if(isset($_POST['submit'])){
    $user = User::register($_POST);
    if($user){
        header("Location: login.php?register=1");
    } else {
        $login_error = "<div id='login-error'><div>We could not find this account.</div><a href='reset.php'><small>password reset</small></a></div>";
    }
}

require_once 'header.php';
?>

<div class="" id="login-container">
    <form action="" method="post">
      <h1>Register</h1>
        <hr>
        <h3>Teamsource needs your help. Register now!</h3>
        <?=$login_error?>
        <input type="text" name="first_name" placeholder="First Name" required><br>
        <input type="text" name="last_name" placeholder="Last Name" required><br>
        <input type="text" name="email" placeholder="Email" value="<?php if(isset($_POST['email'])) echo $_POST['email']; ?>"><br>
        <input type="password" name="password" placeholder="Password" required><br>
        <input type="password" name="password2" placeholder="Repeat Password" required><br>
        <br>
        <strong>I have a lot of experience with...</strong> <br>
        <input type="checkbox" name="design"><label class="checkbox" for="design">Design</label>&nbsp;
        <input type="checkbox" name="marketing"><label class="checkbox" for="marketing">Marketing</label><br>
        <input type="checkbox" name="content"><label class="checkbox" for="marketing">Content</label>
        <input type="checkbox" name="development"><label class="checkbox" for="marketing">Development</label><br>
        <input type="hidden" name="submit" value="1">
        <br>
        <input type="submit" value="Register">
    </form>
</div>

<?php require_once 'footer.php'; ?>